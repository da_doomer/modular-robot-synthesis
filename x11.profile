# See:
# https://ubuntu.com/blog/installing-ros-in-lxd
config:
  environment.DISPLAY: :0.0
  raw.idmap: both 1000 1000
description: ROS
devices:
  X0:
    path: /tmp/.X11-unix/X0
    source: /tmp/.X11-unix/X0
    type: disk
  root:
    path: /
    pool: default
    type: disk
name: webots

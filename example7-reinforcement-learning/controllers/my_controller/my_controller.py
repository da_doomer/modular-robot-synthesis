"""Dummy robot controller"""
# Native imports
from pathlib import Path
import json
import sys
from typing import Dict
import math
import random

# Webots imports
from controller import Supervisor

# External imports
import torch
import numpy as np

# Project imports
sys.path.insert(1, '../../')
from module_controller import Actor, Critic
from module_controller import DPPGBuffer as Buffer

message_file = Path("../../messages/message.json")
SUCCESS_STATUS = 0

# Open message file
with open(message_file) as fp:
    message = "".join(list(fp))
message_dict = json.loads(message)
print("Read {}".format(str(message_dict)))

root_transform_def = "rootTransform"

# Read message dictionary
# File to write response to main process
response_file = Path(message_dict["response_file"])
# File to write photo
photo_file = Path(message_dict["photo_file"])
# File to write video
video_file = Path(message_dict["video_file"])
# Video length in simulation loops
video_length_in_s = message_dict["video_length_in_s"]
# How long to simulate the robot
simulation_seconds = message_dict["simulation_seconds"]
# Simulation loop at which video recording will start
video_at_second = message_dict["video_at_second"]
# Recording settings
video_settings = message_dict["video_settings"]
# Mapping of List[motor IDs] to "neural network file"
motors_actors_critics_files = message_dict["controller_files"]
# Motor sensor sampling period in milliseconds
sensor_sampling_period_in_ms = message_dict["sensor_sampling_period_in_ms"]
# Target coordinates
target_coords = message_dict["target_coords"]
# How often to update the controller in number of experiences
controller_update_frequency = message_dict["controller_update_frequency"]
# Actor constructor arguments
actor_args = message_dict["actor_args"]
# Critic constructor arguments
critic_args = message_dict["critic_args"]
# How often to act in loops
act_cycle_in_loops = message_dict["act_cycle_in_loops"]

# Initialize the controllers
motors_buffers = list()
actor_file_to_actor: Dict[Path, Actor] = dict()
critic_file_to_actor: Dict[Path, Critic] = dict()
for motor_list, actor_file, critic_file in motors_actors_critics_files:
    if actor_file not in actor_file_to_actor.keys():
        actor = Actor(*actor_args[actor_file])
        actor.load_state_dict(torch.load(actor_file))
        actor_file_to_actor[actor_file] = actor
    if critic_file not in critic_file_to_actor.keys():
        critic = Critic(*critic_args[critic_file])
        critic.load_state_dict(torch.load(critic_file))
        critic_file_to_actor[critic_file] = critic
    actor = actor_file_to_actor[actor_file]
    critic = critic_file_to_actor[critic_file]
    motors_buffers.append((tuple(motor_list), Buffer(actor, critic)))

# Initialize response dictionary
response_dict = dict()

# Execute controller
robot = Supervisor()
timestep = int(robot.getBasicTimeStep())
wall_time = 0
robot.simulationSetMode(Supervisor.SIMULATION_MODE_FAST)
print("Set simulation speed to FAST")
robot.selfCollision = True

# Main loop variables
picture_taken = False
running_in_fast_mode = False
loops = 0
video_stopped = False
video_started = False
last_state_action_pair = dict()
emergency_exit = False
reward_history = list()
normalized_distance_history = list()
heading_error_history = list()


def normalized_current_distance() -> float:
    current_coords = robot.getFromDef(root_transform_def).getPosition()
    distance_to_target = sum(((x1i-x2i)**2 for x1i, x2i in zip(
        current_coords, target_coords)
        ))**(1/len(current_coords))
    return distance_to_target/start_distance


def abs_heading_error() -> float:
    root_transform = robot.getFromDef(root_transform_def)
    # Returns a 1-dim list of len=9
    current_orientation = np.array(root_transform.getOrientation())
    # reshape into a 3x3 rotation matrix
    current_orientation = current_orientation.reshape(3, 3)
    # We need the world relative to the robot, not the
    # other way around.
    # Transpose of rotation matrix is its inverse
    current_orientation = current_orientation.T

    # Target position relative to robot coordinate system
    target_relative_translation = np.array(target_coords)-\
                                  np.array(root_transform.getPosition())
    target_relative_position = np.matmul(
            current_orientation, target_relative_translation
        )

    # In the 'root transform' coordinate system:
    # x  (1, 0,  0): left
    # -y (0, -1, 0): forward
    # z  (0, 0,  1): up
    forward = np.array([0,-1,0])
    # Thus (target 'dot' forward) is 1 when going in the correct direction
    # and decreases to -1 when going in the opposite direction.
    # Thus heading error:
    def normalize(vec):
        return vec/np.sqrt((vec**2).sum())
    heading_error = 1-np.dot(normalize(target_relative_position), forward)
    return heading_error


def reward():
    return abs_heading_error()*20.0


current_coords = robot.getFromDef(root_transform_def).getPosition()
start_distance = sum(((x1i-x2i)**2 for x1i, x2i in zip(
    current_coords, target_coords)
    ))**(1/len(current_coords))


experiences = 0
def act():
    global last_state_action_pair
    global start_distance
    global experiences
    # For each module, read sensor data, feed through the network, act
    for motor_list, buff in motors_buffers:
        motors = [robot.getMotor(mname) for mname in motor_list]
        sensors = [m.getPositionSensor() for m in motors]

        # Build current state
        state = list()
        for sensor in sensors:
            if sensor.getSamplingPeriod() == 0:
                sensor.enable(sensor_sampling_period_in_ms)
                state.append(0)
            else:
                state.append(sensor.getValue())
        current_coords = robot.getFromDef(root_transform_def).getPosition()
        current_orientation = robot.getFromDef(root_transform_def).getOrientation()
        state.extend(current_coords)
        state.extend(current_orientation)
        state.extend(target_coords)

        # If this is not the first loop, form an experience and store it
        if motor_list in last_state_action_pair.keys():
            old_state, old_action = last_state_action_pair[motor_list]
            r = reward()
            experience = (old_state, old_action, r, state)
            buff.record(experience)
            experiences += 1

        # Update controller with accumulated experiences
        if loops > controller_update_frequency and\
                loops % controller_update_frequency == 1:
            buff.learn()

        inp = np.array(state)
        action = buff.actor(inp)

        # Store (state, action) to form an experience in the next loop
        last_state_action_pair[motor_list] = (state, action)

        # Controller may return NaN values, skip them
        for i, motor in enumerate(motors):
            val = float(action[i])
            if math.isnan(val):
                continue
            motor.setPosition(val)


# Main loop:
# - perform simulation steps for `simulation_seconds` simulated seconds
while not emergency_exit and\
        robot.step(timestep) != -1 and\
        wall_time/1000 < simulation_seconds:
    wall_time += timestep
    wall_time_s = wall_time/1000
    current_coords = robot.getFromDef(root_transform_def).getPosition()

    # If robot fell off, exit
    if current_coords[1] < -100:
        emergency_exit = True
        continue

    # If robot is reached target coords (10cm), exit
    distance_to_target = sum(((x1i-x2i)**2 for x1i, x2i in zip(
        current_coords, target_coords)
        ))**(1/len(current_coords))
    if distance_to_target < 0.10:
        emergency_exit = True
        continue

    # Print statistics
    if loops % 100 == 0:
        reward_history.append(reward())
        normalized_distance_history.append(normalized_current_distance())
        heading_error_history.append(abs_heading_error())
        print("Simulated {}s/{}s".format(
            round(wall_time/100)/10,
            simulation_seconds)
            )
        print("Target coords: {}".format(str(target_coords)))
        print("Current coords: {}".format(str(current_coords)))
        print("Reward: {}".format(reward_history[-1]))
        print("Normalized distance: {}".format(normalized_distance_history[-1]))
        print("Absolute heading error: {}".format(heading_error_history[-1]))
        print("Lived experiences: {}".format(experiences))

    # Take pictures and video
    if wall_time_s >= video_at_second\
            and not video_started\
            and video_length_in_s > 0:
        robot.simulationSetMode(Supervisor.SIMULATION_MODE_RUN)
        print("Set simulation speed to RUN")
        robot.movieStartRecording(str(video_file), *video_settings)
        video_start = wall_time
        video_started = True
    if video_started:
        robot.exportImage(str(photo_file), 100)
    if video_started\
       and wall_time > video_start + video_length_in_s*1000\
       and not video_stopped:
        robot.movieStopRecording()
        robot.simulationSetMode(Supervisor.SIMULATION_MODE_FAST)
        print("Set simulation speed to FAST")
        video_stopped = True

    if loops % act_cycle_in_loops == 0:
        act()

    loops += 1


# Write trained neural networks to their original files
for actor_file, actor in actor_file_to_actor.items():
    torch.save(actor.state_dict(), actor_file)
    print("Updated {}".format(actor_file))
for critic_file, critic in actor_file_to_actor.items():
    torch.save(critic.state_dict(), critic_file)
    print("Updated {}".format(critic_file))

# Write response message
response_dict["reward_history"] = reward_history
response_dict["normalized_distance_history"] = normalized_distance_history
response_dict["heading_error_history"] = heading_error_history
response_dict["simulation_time_in_s"] = wall_time/1000
response_message = json.dumps(response_dict)
with open(response_file, "wt") as fp:
    fp.write(response_message)

# Spin lock until movie finishes exporting
while not robot.movieIsReady():
    pass
# Quit simulation when movie finishes exporting
robot.simulationQuit(SUCCESS_STATUS)

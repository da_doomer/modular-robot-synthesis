# Modules

Here we introduce reinforcement learning to the module (i.e. sub-assemblies)
controllers.

The pipeline in this example thus looks like this:

1. Sample robot using the pre-defined modules.

2. Assign each module its corresponding learned controller (creating one the
 first time a new module is seen).

3. Place robots in Webots scene.

4. Send the simulator a message with target position, simulation length,
	 controllers, etc.

5. Simulate the robot.

5.1. At each simulation step: loop through the modules and use the corresponding
controller to engage the actuators.

5.2. During simulation train the controllers using reinforcement learning.

5.3. At the end of the simulation write a response JSON for the main process.

6. Store the robot for later analysis.

## Reinforcement learning

In this example we implement an Actor-Critic method to train the
controllers, which are parametrized with a neural network. The
algorithm we employ (Deep Deterministic Policy Gradient, DPPG) is proposed in
[Continuous Control with Deep Reinforcement
Learning](https://arxiv.org/pdf/1509.02971.pdf), Lillicrap et Hunt et al.,
2016.

For a summary of reinforcement-learning algorithms look
[here](https://arxiv.org/pdf/1811.12560.pdf) (e.g. page 25 for
fitted Q-learning, page 40 for actor-critic methods). For an example
of the DPPG algorithm in Keras see [here](https://keras.io/examples/rl/ddpg_pendulum/)

**The main idea** in Actor-Critic methods is to have two
approximators: an actor which proposes an action to take based on
the robot state, and a critic which evaluates actions. Essentially,
we train the critic to approximate the reward function and move the
actor in the direction of the gradient of the critic.

The DPPG algorithm constructs copies `Actor'` and `Critic'` of the
`Actor` and `Critic` networks. This new networks
define the target values during training and are updated slowly,
so as to achieve stability in the training process.
Additionally, the algorithm employs a noise process `N` (so that
`Actor'(s) = Actor(s)+N`) to encourage exploration of the `Actor`
policy. The main loop of the DPPG algorithm is summarized as
follows: at state `st`

1. Select action `at = Actor(st) + N`.

2. Execute action `at` and observe reward `rt` and new state
`st+1`.

3. Store tuple `(st, at, rt, st+1)` in `T`.

4. Sample minibatch `(si, ai, ri, si+1)` of size `N` from `T`.

5. Set `yi = ri + g * Critic'(si+1, Actor'(si+1))`.

6. Update `Critic` by minimizing

```
     1
L = --- * sum  (yi - Critic(si, ai))**2
     N     i
```

7. Update `Actor` using the sampled policy gradient (see paper):

```
               1
grad(Actor) = ---  sum  grad(Critic(si,Actor(si))) * grad(Actor(si))
               N    i
```

8. Update `Actor'` and `Critic'` parameters slightly towards `Actor` and
`Critic` (see paper):

```
Actor' = tau * Actor + (1 - tau) * Actor'
```

```
Critic' = tau * Critic + (1 - tau) * Critic'
```

## Instructions

1. Make sure you have the normal (not snap) version of Webots.

2. `poetry install`

3. `poetry shell`

4. `python main.py`

At the end the [pictures](pictures) folder will contain the images taken during
the pipeline.

See the previous examples for details.

## Examples

Some images taken during a pipeline run:

![Example 0](./pictures/picture0.jpg)

![Example 1](./pictures/picture1.jpg)

![Example 2](./pictures/picture2.jpg)

![Example 3](./pictures/picture3.jpg)

![Example 4](./pictures/picture4.jpg)

![Example 5](./pictures/picture5.jpg)

![Example 6](./pictures/picture6.jpg)

![Example 7](./pictures/picture7.jpg)

![Example 8](./pictures/picture8.jpg)

![Example 9](./pictures/picture9.jpg)

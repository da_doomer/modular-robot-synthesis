"""Analyze the responses sent by the simulator"""

# Native imports
from pathlib import Path
from typing import List
from typing import Dict
from typing import Tuple


# External imports
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.axes import Axes
import numpy as np
from scipy.interpolate import interp1d
from matplotlib import cm
from matplotlib.collections import PolyCollection


def flatten(l1: List[List[float]]) -> List[float]:
    return [number for sublist in l1 for number in sublist]


def plot_list_graphs(graphs: List[List[float]], minimum_z=None) -> Tuple[Figure, Axes]:
    colors = cm.get_cmap('PuBu')(np.linspace(0,1,num=len(graphs)))
    f, ax = plt.subplots(subplot_kw=dict(projection='3d'))
    verts = list()
    if minimum_z is not None:
        for graph in graphs:
            for i in range(len(graph)):
                graph[i] = max(graph[i], minimum_z)
    graphs_min = min(flatten(graphs))
    for i in range(len(graphs)):
        yi = np.arange(0,len(graphs[i])-1,.01)
        xi = [i]*len(yi)
        zi = interp1d(list(range(len(graphs[i]))), graphs[i], kind='cubic')(yi)
        ax.plot(yi,xi,zi,color=colors[i])
        def polygon_under_graph(xlist, ylist):
            """
            Construct the vertex list which defines the polygon filling the space under
            the (xlist, ylist) line graph.  Assumes the xs are in ascending order.
            """
            return [
                    (xlist[0], graphs_min),
                    *zip(xlist, ylist),
                    (xlist[-1], graphs_min)
                ]
        verts.append(polygon_under_graph(yi, zi))
    poly = PolyCollection(verts, facecolors=colors, alpha=1)
    ax.add_collection3d(poly, zs=range(len(graphs)), zdir='y')
    return f, ax


class PDFManager:
    """Thread safe representation of a PDF file"""
    def __init__(self, pdf_dest):
        from threading import RLock
        self.pdf_dest = pdf_dest
        self.lock = RLock()

    def add_pdf(self, pdf, bookmark = None):
        """Appends pdf to pdf_dest. If the file
        did not exist it is created."""
        from os import remove, rename
        from PyPDF2 import PdfFileMerger
        from os.path import isfile
        with self.lock:
            pdf_dest = self.pdf_dest
            filename3 = "./temp3.pdf"
            merger = PdfFileMerger()
            if isfile(pdf_dest): merger.append(pdf_dest)
            if bookmark is not None: merger.append(pdf, bookmark = bookmark)
            else: merger.append(pdf)
            merger.write(filename3)
            merger.close()
            rename(filename3, pdf_dest)

    def add_fig(self, f, show = False):
        """Convenience function. Adds the plot f to the pdf"""
        from os import remove, rename
        with self.lock:
            fig_pdf = "./fig.pdf"
            f.tight_layout()
            f.savefig(fig_pdf)
            self.add_pdf(fig_pdf, self.pdf_dest)
            remove(fig_pdf)
            f.clear()
            plt.close(f)


def analyze_responses_to_pdf(
        filename: Path,
        responses: List[Dict],
    ) -> None:
    # Build PDF manager, overwriting previous file if necessary
    try: remove(pdf_filename)
    except: pass
    pdf_manager = PDFManager(filename)

    # Analyze reward histories
    reward_histories = [response['reward_history'] for response in responses]
    fig, ax = plot_list_graphs(reward_histories)
    ax.set_zlabel("reward")
    ax.set_xlabel('time')
    ax.set_ylabel('simulation')
    pdf_manager.add_fig(fig)

    # Analyze normalized distance histories
    normalized_distances = [response['normalized_distance_history']
                            for response in responses]
    for li in normalized_distances:
        for i in range(len(li)):
            li[i] = -1*li[i]
    fig, ax = plot_list_graphs(normalized_distances, minimum_z=-2.0)
    ax.set_zlabel("minus normalized distance")
    ax.set_xlabel('time')
    ax.set_ylabel('simulation')
    pdf_manager.add_fig(fig)

    # Analyze heading error histories
    heading_error_histories = [response['heading_error_history']
                            for response in responses]
    for li in heading_error_histories:
        for i in range(len(li)):
            li[i] = -1*li[i]
    fig, ax = plot_list_graphs(heading_error_histories)
    ax.set_zlabel("minus heading error distance")
    ax.set_xlabel('time')
    ax.set_ylabel('simulation')
    pdf_manager.add_fig(fig)

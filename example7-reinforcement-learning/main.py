"""This example samples from Robogrammar and places the robot in a procedurally
generated terrain.

See README.md for details.
"""
# Native imports
import subprocess
import json
import pickle
from typing import Dict
from typing import List
from typing import Tuple
from pathlib import Path

# Local imports
from grammar import Terminals as T
from grammar import TreeNode
from grammar import expand_node_open_leaves
from grammar import create_default_expansion_rules
from grammar import expand_expansion_rules
from grammar import id_actuators_uniquely
from grammar import build_root_typeid_to_actuator_ids_mapping
from terrain import random_webots_terrain
from module_controller import Actor, Critic
from webotizer import RobotWebotizer
from webotizer import SceneWebotizer
from responses import analyze_responses_to_pdf

# External imports
import torch

# File to write the world
output_file = Path("worlds/generated.wbt")
# File to write message to the simulator process
message_file = Path("messages/message.json")
# File to write response to the main process
response_file_template = "messages/response{}.json"
# File to write response analysis
response_analysis_file = "messages/responses.pdf"
# File to write photo
photo_file_template = "pictures/picture{}.jpg"
# File to write video
video_file_template = "pictures/video{}.mp4"
# File to write the robot trees
robot_file_template = "messages/robot{}.pickle"
# Module neural network controller Actor file
actor_file_template = "messages/neuralactor{}.torch"
# Module neural network controller Critic file
critic_file_template = "messages/neuralcritic{}.torch"
# Webots command (must be in $PATH)
webots_command = "webots"
# Webots options, as a list of strings
webots_options = ["--stdout", "--stderr", "--batch"]
# How long to simulate the robot
simulation_seconds = 10000
# Video length in simulation loops. 0 -> no video nor photo
video_length_in_s = 0
# Simulation loop at which video recording will start (photo will be taken at
# roughly the same time)
video_at_second = simulation_seconds - video_length_in_s*2
# Video settings (width, height, codec=0, quality=1-100, acc=1, caption)
video_settings = [600, 600, 0, 100, 1, False]
# Simulations to perform
simulations = 100
# Motor sensor sampling period in milliseconds
sensor_sampling_period_in_ms = 100
# How often to update the controller in number of experiences
controller_update_frequency = 128
# How often to act in loops
act_cycle_in_loops = 5

# Add some dummy modules to expansions rules
expansion_rules = create_default_expansion_rules()
modules = list()

# Dummy module: torso with legs
foot = TreeNode(T.LimbLinkShort)
leg = TreeNode(T.LimbElbowJoint, [foot])
leg = TreeNode(T.LimbLinkShort, [leg])
leg = TreeNode(T.LimbRollJoint, [leg])
leg = TreeNode(T.LimbLinkShort, [leg])
leg = TreeNode(T.LimbKneeJoint, [leg])
leg = TreeNode(T.LimbLinkShort, [leg])
leg = TreeNode(T.MountLink, [leg])
leg = TreeNode(T.Connector, [leg])
legged_link_joint = TreeNode(T.BodyRollJoint, [])
legged_link = TreeNode(T.BodyLink, [leg, legged_link_joint])
legged_link.compress(open_leaves=[legged_link_joint])
legged_link.typeid = "link_n_limbs"

expansion_rules = expand_expansion_rules(expansion_rules, legged_link)
modules.append(legged_link)

# Dummy module: torso with wheels
foot2 = TreeNode(T.Wheel)
leg2 = TreeNode(T.WheelJoint, [foot2])
leg2 = TreeNode(T.LimbLinkShort, [leg2])
leg2 = TreeNode(T.LimbKneeJoint, [leg2])
leg2 = TreeNode(T.LimbLinkShort, [leg2])
leg2 = TreeNode(T.LimbKneeJoint, [leg2])
leg2 = TreeNode(T.LimbLinkShort, [leg2])
leg2 = TreeNode(T.MountLink, [leg2])
leg2 = TreeNode(T.Connector, [leg2])
legged_link_joint2 = TreeNode(T.BodyRollJoint, [])
legged_link2 = TreeNode(T.BodyLink, [leg2, legged_link_joint2])
legged_link2.compress(open_leaves=[legged_link_joint2])
legged_link2.typeid = "link_n_wheels"

expansion_rules = expand_expansion_rules(expansion_rules, legged_link2)
modules.append(legged_link2)

# Dummy module: empty torso (end the torso chain)
empty_link = TreeNode(T.BodyLink, [])
empty_link.compress([])
empty_link.typeid = "link_empty"

expansion_rules = expand_expansion_rules(expansion_rules, empty_link)
modules.append(empty_link)

# List of module controller files
module_controller_files: Dict[str, Tuple[str, str]] = dict()
# Critic and Actor constructor arguments
actor_args = dict()
critic_args = dict()

# Simulation response messages
response_dicts: List[Dict] = list()


def myprint(message: str) -> None:
    print("MAIN: {}".format(message))


def run_webots_with_file(filename: Path) -> None:
    """Runs webots with `filename`. Blocks until the process completes."""
    command = [webots_command, *webots_options, str(filename)]
    subprocess.run(command)
    return None


for i in range(simulations):
    myprint("Simulation {}/{}".format(i+1, simulations))

    # Sample terrain
    webots_terrain, target_coords = random_webots_terrain()

    # Sample robot non-trivial
    robot = expand_node_open_leaves(
            legged_link,
            expansion_rules=expansion_rules,
            only_compressed_trees=True,
            )
    while robot.size < 10:
        robot = expand_node_open_leaves(
                legged_link,
                expansion_rules=expansion_rules,
                only_compressed_trees=True,
                )

    # Name the actuators uniquely and cluster into typeids
    robot = id_actuators_uniquely(robot)
    typeids_to_actuators_id = build_root_typeid_to_actuator_ids_mapping(robot)

    # Build a list of (list of actuators, controller file) pairs
    controller_files_message = list()
    for typeid, actuator_ids_lists in typeids_to_actuators_id.items():
        if typeid not in module_controller_files.keys():
            actor_file = str(Path(actor_file_template.format(
                    typeid
                )).resolve())
            critic_file = str(Path(critic_file_template.format(
                    typeid
                )).resolve())
            # State: motor state
            # + (x,y,z) current coords
            # + 3x3 orientation matrix
            # + (x,y,z) target coords
            state_size = len(actuator_ids_lists[0])+3+9+3
            # Action: next_motor state
            action_size = len(actuator_ids_lists[0])
            # Reward: always a single number
            reward_size = 1
            actor_args[actor_file] = (
                    state_size,
                    action_size,
                )
            critic_args[critic_file] = (
                    state_size+action_size,
                    reward_size,
                )
            actor = Actor(*actor_args[actor_file])
            critic = Critic(*critic_args[critic_file])
            torch.save(actor.state_dict(), actor_file)
            torch.save(critic.state_dict(), critic_file)
            myprint("Wrote {}".format(actor_file))
            module_controller_files[typeid] = (
                    actor_file, critic_file
                )

        # Find the controller file path for this typeid
        actor_file, critic_file = module_controller_files[typeid]

        # Tell the simulator to use this controller for this lists of motors
        for actuator_ids_list in actuator_ids_lists:
            controller_files_message.append((
                    actuator_ids_list,
                    actor_file,
                    critic_file,
                ))

    webots_robot: str = RobotWebotizer().astnode_to_webots(robot)

    # Place terrain and robot in Webots scene
    webots_scene = SceneWebotizer().scene_to_webots(
            webots_robot, webots_terrain, target_coords
        )

    with open(output_file, "wt") as fp:
        fp.write(webots_scene)
    myprint("wrote {}".format(output_file))

    # Write file that will be read during simulation
    response_file = Path(response_file_template.format(str(i)))
    photo_file = Path(photo_file_template.format(str(i)))
    video_file = Path(video_file_template.format(str(i)))
    message_dict = {
            "ID": 0,
            "response_file": str(response_file.resolve()),
            "photo_file": str(photo_file.resolve()),
            "video_file": str(video_file.resolve()),
            "simulation_seconds": simulation_seconds,
            "video_at_second": video_at_second,
            "video_length_in_s": video_length_in_s,
            "video_settings": video_settings,
            "controller_files": controller_files_message,
            "sensor_sampling_period_in_ms": sensor_sampling_period_in_ms,
            "target_coords": target_coords,
            "controller_update_frequency": controller_update_frequency,
            "actor_args": actor_args,
            "critic_args": critic_args,
            "act_cycle_in_loops": act_cycle_in_loops,
        }
    message_to_robot = json.dumps(message_dict)

    with open(message_file, "wt") as fp:
        fp.write(message_to_robot)
    myprint("wrote {}".format(message_file))

    # Launch Webots process
    myprint("Running Webots with file {}".format(str(output_file)))
    run_webots_with_file(output_file)

    # Read statistics in response
    with open(response_file, "rt") as fp:
        response = "".join(list(fp))
    response_dict = json.loads(response)
    response_dicts.append(response_dict)
    myprint("Response: {}".format(str(response_dict)))

    # Store robot structure for later analysis
    robot_file = robot_file_template.format(str(i))
    with open(robot_file, 'wb') as fpb:
        pickle.dump(robot, fpb)
    myprint("Wrote {}".format(robot_file))

    del robot

# Write responses analysis to PDF
analyze_responses_to_pdf(response_analysis_file, response_dicts)

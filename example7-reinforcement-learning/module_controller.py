"""Implementation of the Deep Deterministic Policy Gradient, proposed in
[Continuous Control With Deep Reinforcement
Learning](https://arxiv.org/pdf/1509.02971.pdf), 2016.

By the very nature of reinforcement learning this implementation is (if only
slightly) coupled with the simulation, which stores "experiences" in a
`Buffer`. These "experiences" are used so that a `Critic` is trained to
estimate the reward function for a particular action proposed by an `Actor`.
The `Actor` is trained in the direction of the gradient of `Critic`.

See the paper for details, or the accompanying `README.md` for a summary.
"""
import numpy as np
from torch import nn
from torch import optim
import torch
import copy
import typing
import math

device = "cuda:0" if torch.cuda.is_available() else "cpu"
State = typing.TypeVar("State")
Action = typing.TypeVar("Action")
Reward = typing.TypeVar("Reward")
Experience = typing.Tuple[State, Action, Reward, State]


class Actor(nn.Module):
    def __init__(
            self,
            ins: int,
            outs: int,
            **kwargs):
        super().__init__(**kwargs)
        self.ins = ins
        self.outs = outs
        self.lin1 = nn.Linear(ins, 100).to(device)
        self.act1 = nn.CELU().to(device)
        self.lin2 = nn.Linear(100, outs).to(device)
        self.forward_count = 0

    def forward(self, x: State) -> Action:
        """Expects a batch numpy array."""
        x = torch.from_numpy(x).float().to(device)
        x = self.lin1(x)
        x = self.act1(x)
        x = self.lin2(x)

        # Add exponentially decreasing noise
        mu, sigma = 0, math.log(self.forward_count+10)**-1 + 0.05
        noise = torch.from_numpy(np.random.normal(mu, sigma, x.size())).float().to(device)
        self.forward_count += 1
        return x + noise


class Critic(nn.Module):
    def __init__(
            self,
            ins: int,
            outs: int,
            **kwargs):
        super().__init__(**kwargs)
        self.ins = ins
        self.outs = outs
        self.lin1 = nn.Linear(ins, 100).to(device)
        self.act1 = nn.CELU().to(device)
        self.lin2 = nn.Linear(100, outs).to(device)

    def forward(self, x: typing.Tuple[State, Action]) -> Reward:
        """ State is always a numpy array, action is always a torch tensor.
        Both are batches."""
        x = torch.cat(
                (
                    torch.from_numpy(x[0]).float().to(device),
                    torch.from_numpy(x[1]).float().to(device),
                ),
                dim=-1
            )
        x = self.lin1(x)
        x = self.act1(x)
        x = self.lin2(x)
        return x


class DPPGBuffer:
    """A `Buffer` stores `(si, ai, ri, si+1)` tuples ("experiences"), where
    `si`, `ai`, `ri`, `si+1` are a state, action, reward and next state
    respectively.

    The `Buffer` then updates the corresponding `Critic` and `Actor`.

    `Buffer`s have a `capacity`. If the number of "experiences" exceeds this
    `capacity` then old experiences will be overwritten.

    This implementation is based on the example at the Keras documentation:
    https://keras.io/examples/rl/ddpg_pendulum/
    """
    def __init__(
            self,
            actor: Actor,
            critic: Critic,
            capacity: int = 100000,  # maximum experiences to store
            batch_size: int = 64,
            gamma: float = 0.1,
            tau: float = 0.1,
            ):
        self.actor = actor
        self.critic = critic
        self.capacity = capacity
        self.batch_size = batch_size
        self.stored_experiences = 0
        self.gamma = gamma
        self.tau = tau

        # Build optimizers
        self.actor_optimizer = optim.Adam(self.actor.parameters())
        self.critic_optimizer = optim.Adam(self.critic.parameters())

        # Create "target" Actor' and Critic' copying parameters
        self.actorp = copy.copy(actor)
        self.actorp.load_state_dict(actor.state_dict())
        self.criticp = copy.copy(critic)
        self.criticp.load_state_dict(critic.state_dict())

        # Instead of list of tuples use arrays for each tuple element
        state_size = actor.ins
        action_size = actor.outs
        self.state_buffer = np.zeros((capacity, state_size))
        self.action_buffer = np.zeros((capacity, action_size))
        self.reward_buffer = np.zeros((capacity, 1))
        self.next_state_buffer = np.zeros((capacity, state_size))

    def record(self, experience: Experience) -> None:
        """Overwrites old experiences if capacity is exceeded"""
        index = self.stored_experiences % self.capacity
        # State is always np array-like
        self.state_buffer[index] = np.array(experience[0])
        # Action is always torch tensor
        self.action_buffer[index] = experience[1].detach().cpu().numpy()
        # Reward is always np array-like
        self.reward_buffer[index] = np.array(experience[2])
        # Next state is always np array-like
        self.next_state_buffer[index] = np.array(experience[3])
        self.stored_experiences += 1

    def learn(self) -> None:
        """Updates the Critic and Actor networks according to the
        DPPG algorithm."""
        # Sample random experiences from the buffer
        record_range = min(self.stored_experiences, self.capacity)
        batch_indices = np.random.choice(record_range, self.batch_size)
        state_batch = self.state_buffer[batch_indices]
        action_batch = self.action_buffer[batch_indices]
        reward_batch = self.reward_buffer[batch_indices]
        next_state_batch = self.next_state_buffer[batch_indices]

        # Update Critic
        self.critic_optimizer.zero_grad()
        reward_batch_t = torch.from_numpy(reward_batch).float().to(device)
        y = reward_batch_t + self.gamma * self.criticp((
                next_state_batch, self.actorp(next_state_batch).detach().cpu().numpy()
            ))
        critic_loss = torch.mean(torch.square(
                y - self.critic((state_batch, action_batch))
            ))
        critic_loss.backward()
        self.critic_optimizer.step()

        # Update Actor
        self.actor_optimizer.zero_grad()
        actor_loss = -torch.mean(
                self.critic((state_batch, action_batch))
            )
        actor_loss.backward()
        self.actor_optimizer.step()

        # Update Critic' and Actor' slightly towards Critic and Actor
        for ap, a in zip(self.actorp.parameters(), self.actor.parameters()):
            ap = self.tau * a + (1 - self.tau) * ap
        for cp, c in zip(self.criticp.parameters(), self.critic.parameters()):
            cp = self.tau * c + (1 - self.tau) * cp

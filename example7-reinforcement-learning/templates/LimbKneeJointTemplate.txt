HingeJoint {
  jointParameters HingeJointParameters {
    position 0
    axis 1 0 0
    anchor {{translation}}
  }
  device [
    RotationalMotor {
    name "{{name}}"
    }
    PositionSensor {
      name "{{name}}_sens"
    }
  ]
  endPoint {{endPoint}}
}
Transform {
  rotation 0 0 1 1.57
  translation {{translation}}
  children [
    LimbKneeJointShape {
    }
  ]
}

Transform {
  rotation {{rotation}}
  children [
    Solid {
      translation {{translation}}
      children [
        {{children}}
        ConnectorShape {
        }
      ]
      boundingObject ConnectorShape {
      }
      physics Physics {
      }
      name "{{name}}"
    }
  ]
}

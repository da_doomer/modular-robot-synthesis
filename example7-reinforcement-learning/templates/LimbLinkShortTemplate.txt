Solid {
  translation {{translation}}
  rotation {{rotation}}
  children [
    LimbLinkShortShape {
    }
    {{children}}
  ]
  boundingObject LimbLinkShortShape {
  }
  physics Physics {
  }
  name "{{name}}"
}

Solid {
  translation {{translation}}
  children [
    DEF terrain Shape {
      appearance Plaster {
        textureTransform TextureTransform {
          scale {{texture_scale}}
        }
        IBLStrength 2
      }
      geometry ElevationGrid {
        height [
          {{height}}
        ]
        xDimension {{xDimension}}
        zDimension {{zDimension}}
        xSpacing {{xSpacing}}
        zSpacing {{zSpacing}}
      }
    }
  ]
  boundingObject USE terrain
}

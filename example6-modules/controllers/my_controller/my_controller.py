"""Dummy robot controller"""
# Native imports
from pathlib import Path
import json
import sys
from typing import Dict
import math

# Webots imports
from controller import Supervisor

# External imports
import torch
import numpy as np

# Project imports
sys.path.insert(1, '../../')
from module_controller import Controller

message_file = Path("../../messages/message.json")
SUCCESS_STATUS = 0

# Open message file
with open(message_file) as fp:
    message = "".join(list(fp))
message_dict = json.loads(message)
print("Read {}".format(str(message_dict)))

# Read message dictionary
# File to write response to main process
response_file = Path(message_dict["response_file"])
# File to write photo
photo_file = Path(message_dict["photo_file"])
# How long to simulate the robot
simulation_seconds = message_dict["simulation_seconds"]
# Simulation loop at which the photo will be taken
photo_at_loop = message_dict["photo_at_loop"]
# Mapping of List[motor IDs] to "neural network file"
motor_lists_n_controller_files = message_dict["controller_files"]
# Motor sensor sampling period in milliseconds
sensor_sampling_period_in_ms = message_dict["sensor_sampling_period_in_ms"]

# Initialize the controllers
motor_lists_n_controllers = list()
controller_file_to_controller: Dict[Path, Controller] = dict()
for motor_list, controller_file in motor_lists_n_controller_files:
    if controller_file not in controller_file_to_controller.keys():
        controller = Controller(
                ins=len(motor_list),
                outs=len(motor_list),
            )
        controller.load_state_dict(torch.load(controller_file))
        controller_file_to_controller[controller_file] = controller
    controller = controller_file_to_controller[controller_file]
    motor_lists_n_controllers.append((motor_list, controller))

# Initialize response dictionary
response_dict = dict()
response_dict["fitness"] = 10.0

# Execute controller
robot = Supervisor()
timestep = int(robot.getBasicTimeStep())
wall_time = 0
robot.simulationSetMode(Supervisor.SIMULATION_MODE_FAST)

# Main loop:
# - perform simulation steps for `simulation_seconds` simulated seconds
picture_taken = False
running_in_fast_mode = False
loops = 0
while robot.step(timestep) != -1 and wall_time/1000 < simulation_seconds:
    # For each module, read sensor data, feed through the network, act
    for motor_list, controller in motor_lists_n_controllers:
        motors = [robot.getMotor(mname) for mname in motor_list]
        sensors = [m.getPositionSensor() for m in motors]
        sensor_positions = list()
        # Activate sensors the first time
        for sensor in sensors:
            if sensor.getSamplingPeriod() == 0:
                sensor.enable(sensor_sampling_period_in_ms)
                sensor_positions.append(0)
            else:
                sensor_positions.append(sensor.getValue())
        inp = np.array(sensor_positions)
        out = controller(inp)
        # Controller may return NaN values, skip them
        for i, motor in enumerate(motors):
            val = float(out[i])
            if math.isnan(val):
                continue
            motor.setPosition(val)

    # Hack to get the pictures to export correctly:
    # Take pictures for the 10 previous loops at which I was
    # supposed to take the photo.
    # This is so Webots can render graphics, as they were
    # disabled (simulation was running in fast mode).
    if loops == photo_at_loop-10:
        robot.simulationSetMode(Supervisor.SIMULATION_MODE_REAL_TIME)
    if loops > photo_at_loop-10 and loops < photo_at_loop:
        robot.exportImage(str(photo_file), 99)
    if loops == photo_at_loop:
        robot.simulationSetMode(Supervisor.SIMULATION_MODE_FAST)

    loops += 1

# Write response message (here using dummy statistics)
response_message = json.dumps(response_dict)
with open(response_file, "wt") as fp:
    fp.write(response_message)

# Quit simulation
robot.simulationQuit(SUCCESS_STATUS)

HingeJoint {
  jointParameters HingeJointParameters {
    position 0
    axis 0 0 1
    anchor {{translation}}
  }
  device [
    RotationalMotor {
      name "{{name}}"
    }
    PositionSensor {
      name "{{name}}_sens"
    }
  ]
  endPoint {{endPoint}}
}
Transform {
  rotation 1 0 0 1.57
  translation {{translation}}
  children [
    BodyRollJointShape {
    }
  ]
}

from pathlib import Path
from torch import nn
import torch

device = "cuda:0" if torch.cuda.is_available() else "cpu"

class Controller(nn.Module):
    def __init__(
            self,
            ins: int,
            outs: int,
            **kwargs):
        super().__init__(**kwargs)
        self.lin1 = nn.Linear(ins, 100)
        self.act1 = nn.CELU()
        self.lin2 = nn.Linear(100, outs)

    def forward(self, x):
        """Expects a numpy array."""
        x = torch.from_numpy(x).float().to(device)
        x = self.lin1(x)
        x = self.act1(x)
        x = self.lin2(x)
        return x

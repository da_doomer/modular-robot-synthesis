import enum
from typing import Dict
from typing import List
from typing import Union
import collections
from collections import defaultdict
import random
import copy


class Terminals(enum.Enum):
    # Body
    BodyLink = enum.auto()
    BodyRollJoint = enum.auto()
    BodyRigidJoint = enum.auto()
    BodyTwistJoint = enum.auto()

    # Legs
    Connector = enum.auto()
    MountLink = enum.auto()
    LimbLinkShort = enum.auto()
    LimbLinkLong = enum.auto()
    LimbRigidJoint = enum.auto()
    LimbRollJoint = enum.auto()
    LimbKneeJoint = enum.auto()
    LimbElbowJoint = enum.auto()
    WheelJoint = enum.auto()
    Wheel = enum.auto()


T = Terminals
Actuators: List[Terminals] = [
        T.BodyRollJoint,
        # T.BodyRigidJoint is not an actuator
        T.BodyTwistJoint,
        T.LimbKneeJoint,
        T.LimbElbowJoint,
        T.LimbRollJoint,
        # T.LimbRigidJoint is not an actuator
        T.WheelJoint,
    ]


class TreeNode(collections.UserList):
    """Nodes have a token attribute, and data: a list of children.

    Nodes also have an ID, which may be None, or an integer.

    TreeNodes can identify their actuators.

    TreeNodes can be compressed. When compressed, the TreeNode will represent
    a module, and will have a list of `open_leaves`, representing the
    leaves that can be extended for sampling purposes. When compressed, a
    recursive traversal will make every child node points toward the
    `root`. The traversal will not mark nodes who are themselves compressed.
    """
    def __init__(self,
                 token: Terminals,
                 children: List['TreeNode'] = list(),
                 ID: str = None,
                 open_leaves: List['TreeNode'] = list(),
                 compressed: bool = False,
                 typeid: str = None,
                 ):
        super().__init__(self)
        self.token = token
        self.data.extend(children)
        self.ID = ID
        self._open_leaves: List['TreeNode'] = open_leaves
        self.compressed = compressed
        self.root = self
        self.typeid = typeid

    @property
    def size(self):
        if self.is_leaf:
            return 1
        else:
            return 1+sum([child.size for child in self.children])

    @property
    def is_leaf(self):
        return len(self.data) == 0

    @property
    def children(self):
        return self.data

    @children.setter
    def children(self, value):
        self.data = value

    @property
    def actuators(self) -> List['TreeNode']:
        recursive_actuators: List[TreeNode] = list()
        if self.token in Actuators:
            recursive_actuators.append(self)
        for child in self.children:
            if child.root == self.root:
                recursive_actuators.extend(
                        child.actuators
                    )
        return recursive_actuators

    def _recursive_root_mark(self, root):
        """Recursively marks every child to point to `root`, while
        avoiding to visit compressed childs. If root is `None` then
        `self.root` is set to `self`."""
        if root is None:
            self.root = self
        else:
            self.root = root
        # If current node is an open leaf stop
        if self in self.root.open_leaves:
            return
        # Else, recursively mark every children even if it was previously
        # a compressed tree.
        for child in self.children:
            child._recursive_root_mark(root)

    def compress(self, open_leaves: List['TreeNode']):
        """The given list should be a list of nodes belonging to `self`."""
        self._open_leaves = open_leaves
        self.compressed = True
        self._recursive_root_mark(self)

    def uncompress(self):
        self._recursive_root_mark(None)
        self.compressed = False
        self._open_leaves = list()

    @property
    def open_leaves(self):
        """If tree is compressed, `open_leaves` is the list given when
        `compressed(open_leaves)`. Else, `open_leaves` is `self.children`."""
        if self.compressed:
            return self._open_leaves
        return self.children

    def __repr__(self):
        str_repr = "(typeid: {}, id: {}, token: {}, children: {}): \n {}".format(
                str(self.typeid),
                str(self.ID),
                repr(self.token),
                str(len(self.data)),
                repr(self.data),
            )
        if self.compressed:
            str_repr = "Compressed " + str_repr
        return str_repr


# Expansion rules (i.e. given this Terminal, what are their possible children)
TM = Union[T, TreeNode]
ExpansionRules = Dict[TM, List[List[TM]]]


def expand_expansion_rules(
        expansion_rules: ExpansionRules,
        compressed_tree_node: TreeNode,
        ) -> ExpansionRules:
    """Inserts `compressed_tree_node` to `expansion_rules`, so that whenever
    the root of the compressed tree node appeared, the compressed tree node
    can now be inserted."""
    new_rules = dict(expansion_rules)
    for left_side, right_sides in expansion_rules.items():
        for right_side in right_sides:
            if compressed_tree_node.token in right_side:
                new_right_side: List[Union[Terminals, TreeNode]] = list()
                for item in right_side:
                    if isinstance(item, Terminals) and\
                            item is compressed_tree_node.token:
                        new_right_side.append(compressed_tree_node)
                    else:
                        new_right_side.append(item)
                new_rules[left_side].append(new_right_side)
    return new_rules


def create_default_expansion_rules() -> ExpansionRules:
    return {
        # Body
        T.BodyLink: [
                [],
                [T.Connector],
                [T.Connector, T.BodyRollJoint],
                [T.Connector, T.BodyRigidJoint],
                [T.Connector, T.BodyTwistJoint],
                [T.BodyRollJoint],
                [T.BodyRigidJoint],
                [T.BodyTwistJoint],
            ],
        T.BodyRollJoint: [[T.BodyLink]],
        T.BodyRigidJoint: [[T.BodyLink]],
        T.BodyTwistJoint: [[T.BodyLink]],

        # Legs
        T.Connector: [[T.MountLink]],
        T.MountLink: [  # SLIGHT MODIFICATION TO ROBOGRAMMAR IN THIS RULE
                [T.LimbLinkLong],  # Instead of MountLink -> Joint,
                [T.LimbLinkShort],  # MountLink -> LimbLink
            ],
        T.LimbLinkShort: [
                [],
                [T.LimbRigidJoint],
                [T.LimbRollJoint],
                [T.LimbKneeJoint],
                [T.LimbElbowJoint],
                [T.WheelJoint],
            ],
        T.LimbLinkLong: [
                [],
                [T.LimbRigidJoint],
                [T.LimbRollJoint],
                [T.LimbKneeJoint],
                [T.LimbElbowJoint],
                [T.WheelJoint],
            ],
        T.LimbRigidJoint: [
                [T.LimbLinkLong],
                [T.LimbLinkShort],
            ],
        T.LimbRollJoint: [
                [T.LimbLinkLong],
                [T.LimbLinkShort],
            ],
        T.LimbKneeJoint: [
                [T.LimbLinkLong],
                [T.LimbLinkShort],
            ],
        T.LimbElbowJoint: [
                [T.LimbLinkLong],
                [T.LimbLinkShort],
            ],
        T.WheelJoint: [
                [T.Wheel],
            ],
        T.Wheel: [
                [],
            ],
        }


def sample_terminal_sentence(
            token: Terminals = T.BodyLink,
            expansion_rules: ExpansionRules = None,
            only_compressed_trees: bool = False,
        ) -> TreeNode:
    """Receives a Terminal token and samples a (partial) sentence from
    Robogrammar which has a the token as its root. If the token is e.g.
    a BodyLink, then the returned sentence is a complete sentence.

    If `only_compressed_trees==True` then only rules that are completely
    made of compressed `NodeTree`s can be choosed for expansion."""
    # Choose an expansion rule
    if expansion_rules is None:
        expansion_rules = create_default_expansion_rules()

    if only_compressed_trees:
        available_rules = list()
        for right_side in expansion_rules[token]:
            only_compressed_trees_in_rule = True
            for item in right_side:
                if isinstance(item, Terminals):
                    only_compressed_trees_in_rule = False
            if only_compressed_trees_in_rule:
                available_rules.append(right_side)
    else:
        available_rules = expansion_rules[token]

    chosen_rule = random.choice(available_rules)

    # Recursively sample terminal sentences
    children = list()
    for child in chosen_rule:
        if isinstance(child, TreeNode):
            # If is module then extend child open leaves
            appendage = copy.deepcopy(child)
            appendage = expand_node_open_leaves(
                    appendage,
                    expansion_rules=expansion_rules,
                    only_compressed_trees=only_compressed_trees,
                    )
        else:
            # If is not TreeNode then extend child directly
            appendage = sample_terminal_sentence(
                        child,
                        expansion_rules=expansion_rules,
                        only_compressed_trees=only_compressed_trees,
                    )
        children.append(appendage)

    return TreeNode(token, children)


def expand_node_open_leaves(
            node: TreeNode,
            expansion_rules: ExpansionRules = None,
            only_compressed_trees: bool = False,
            ):
    node = copy.deepcopy(node)
    for leaf in node.open_leaves:
        leaf.children = sample_terminal_sentence(
                leaf.token,
                expansion_rules=expansion_rules,
                only_compressed_trees=only_compressed_trees,
            ).children
    return node


def id_actuators_uniquely(
        node: TreeNode,
        used_names: List[int] = [-1],
        ):
    """Name actuator nodes in the `TreeNode` uniquely. Nodes should
    have a typeid."""
    node = copy.deepcopy(node)
    if node.typeid is None:
        raise Exception("The given node has None typeid")
    for actuator in node.actuators:
        current_name = max(used_names)+1
        used_names.append(current_name)
        actuator.ID = "{}.{}".format(str(node.typeid), str(current_name))
    if node.compressed:
        for leaf in node.open_leaves:
            for i, child in enumerate(leaf.children):
                leaf.children[i] = id_actuators_uniquely(
                        child,
                        used_names=used_names,
                    )
    else:
        for i, child in enumerate(node.children):
            node.children[i] = id_actuators_uniquely(
                    child,
                    used_names=used_names,
                )
    return node


def _build_root_typeid_to_actuator_ids_mapping(
        node: TreeNode,
        mapping: Dict[str, List[List[str]]],
        ) -> Dict[str, List[List[str]]]:
    """Helper function to simplify function usage and avoid default
    parameter value state (python only instantiates default parameters
    once)."""
    if node.typeid is None:
        raise Exception("The given node has None typeid")
    this_node_actuators = list()
    for actuator in node.actuators:
        if actuator.ID is None:
            raise Exception("The given node has an actuator with None ID.")
        this_node_actuators.append(actuator.ID)
    if len(this_node_actuators) > 0:
        mapping[node.typeid].append(this_node_actuators)
    for leaf in node.open_leaves:
        for child in leaf.children:
            mapping = _build_root_typeid_to_actuator_ids_mapping(
                    child,
                    mapping=mapping,
                )
    return mapping


def build_root_typeid_to_actuator_ids_mapping(
        node: TreeNode,
        ) -> Dict[str, List[List[str]]]:
    """Every root node should have a typeid. Every actuator should have a
    unique name.

    This returns a mapping from `typeid` `str`s to list of list of
    actuator IDs.
    """
    mapping: Dict[str, List[List[str]]] = defaultdict(list)
    return _build_root_typeid_to_actuator_ids_mapping(node, mapping)

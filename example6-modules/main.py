"""This example samples from Robogrammar and places the robot in a procedurally
generated terrain.

See README.md for details.
"""
# Native imports
import subprocess
import json
import pickle
from typing import Dict
from pathlib import Path

# Local imports
from grammar import Terminals as T
from grammar import TreeNode
from grammar import expand_node_open_leaves
from grammar import create_default_expansion_rules
from grammar import expand_expansion_rules
from grammar import id_actuators_uniquely
from grammar import build_root_typeid_to_actuator_ids_mapping
from terrain import random_webots_terrain
from module_controller import Controller
from webotizer import RobotWebotizer
from webotizer import SceneWebotizer

# External imports
import torch

# File to write the world
output_file = Path("worlds/generated.wbt")
# File to write message to the simulator process
message_file = Path("messages/message.json")
# File to write response to the main process
response_file_template = "messages/response{}.json"
# File to write photo
photo_file_template = "pictures/picture{}.jpg"
# File to write the robot trees
robot_file_template = "messages/robot{}.pickle"
# Module neural network controller file
module_neural_controller_file_template = "messages/neuralcontrol{}.torch"
# Webots command (must be in $PATH)
webots_command = "webots"
# How long to simulate the robot
simulation_seconds = 10
# Simulation loop at which the photo will be taken
photo_at_loop = 100
# Simulations to perform
simulations = 10
# Motor sensor sampling period in milliseconds
sensor_sampling_period_in_ms = 100

# Add some dummy modules to expansions rules
expansion_rules = create_default_expansion_rules()
modules = list()

# Dummy module: torso with legs
foot = TreeNode(T.LimbLinkShort)
leg = TreeNode(T.LimbElbowJoint, [foot])
leg = TreeNode(T.LimbLinkShort, [leg])
leg = TreeNode(T.MountLink, [leg])
leg = TreeNode(T.Connector, [leg])
legged_link_joint = TreeNode(T.BodyRollJoint, [])
legged_link = TreeNode(T.BodyLink, [leg, legged_link_joint])
legged_link.compress(open_leaves=[legged_link_joint])
legged_link.typeid = "link_n_limbs"

expansion_rules = expand_expansion_rules(expansion_rules, legged_link)
modules.append(legged_link)

# Dummy module: torso with wheels
foot2 = TreeNode(T.Wheel)
leg2 = TreeNode(T.WheelJoint, [foot2])
leg2 = TreeNode(T.LimbLinkShort, [leg2])
leg2 = TreeNode(T.MountLink, [leg2])
leg2 = TreeNode(T.Connector, [leg2])
legged_link_joint2 = TreeNode(T.BodyRollJoint, [])
legged_link2 = TreeNode(T.BodyLink, [leg2, legged_link_joint2])
legged_link2.compress(open_leaves=[legged_link_joint2])
legged_link2.typeid = "link_n_wheels"

expansion_rules = expand_expansion_rules(expansion_rules, legged_link2)
modules.append(legged_link2)

# Dummy module: empty torso (end the torso chain)

empty_link = TreeNode(T.BodyLink, [])
empty_link.compress([])
empty_link.typeid = "link_empty"

expansion_rules = expand_expansion_rules(expansion_rules, empty_link)
modules.append(empty_link)

# List of module controller files
module_controller_files: Dict[str, Path] = dict()


def myprint(message: str) -> None:
    print("MAIN: {}".format(message))


def run_webots_with_file(filename: Path) -> None:
    """Runs webots with `filename`. Blocks until the process completes."""
    command = [webots_command, str(filename)]
    subprocess.run(command)
    return None


for i in range(simulations):
    myprint("Simulation {}/{}".format(i+1, simulations))

    # Sample robot non-trivial
    robot = expand_node_open_leaves(
            legged_link,
            expansion_rules=expansion_rules,
            only_compressed_trees=True,
            )
    while robot.size < 10:
        robot = expand_node_open_leaves(
                legged_link,
                expansion_rules=expansion_rules,
                only_compressed_trees=True,
                )

    # Name the actuators uniquely and cluster into typeids
    robot = id_actuators_uniquely(robot)
    typeids_to_actuators_id = build_root_typeid_to_actuator_ids_mapping(robot)

    # Build a list of (list of actuators, controller file) pairs
    controller_files_message = list()
    for typeid, actuator_ids_lists in typeids_to_actuators_id.items():
        if typeid not in module_controller_files.keys():
            controller_file = module_neural_controller_file_template.format(
                    typeid
                )
            controller = Controller(
                ins=len(actuator_ids_lists[0]),
                outs=len(actuator_ids_lists[0]),
                    )
            torch.save(controller.state_dict(), Path(controller_file))
            myprint("Wrote {}".format(Path(controller_file)))
            module_controller_files[typeid] = Path(controller_file)

        # Find the controller file path for this typeid
        controller_file_path = module_controller_files[typeid]

        # Tell the simulator to use this controller for this lists of motors
        for actuator_ids_list in actuator_ids_lists:
            controller_files_message.append(
                (actuator_ids_list, str(controller_file_path.resolve()))
                    )

    webots_robot: str = RobotWebotizer().astnode_to_webots(robot)

    # Sample terrain
    webots_terrain = random_webots_terrain()

    # Place terrain and robot in Webots scene
    webots_scene = SceneWebotizer().scene_to_webots(
            webots_robot, webots_terrain
        )

    with open(output_file, "wt") as fp:
        fp.write(webots_scene)
    myprint("wrote {}".format(output_file))

    # Write file that will be read during simulation
    response_file = Path(response_file_template.format(str(i)))
    photo_file = Path(photo_file_template.format(str(i)))
    message_dict = {
            "ID": 0,
            "response_file": str(response_file.resolve()),
            "photo_file": str(photo_file.resolve()),
            "simulation_seconds": simulation_seconds,
            "photo_at_loop": photo_at_loop,
            "controller_files": controller_files_message,
            "sensor_sampling_period_in_ms": sensor_sampling_period_in_ms,
        }
    message_to_robot = json.dumps(message_dict)

    with open(message_file, "wt") as fp:
        fp.write(message_to_robot)
    myprint("wrote {}".format(message_file))

    # Launch Webots process
    myprint("Running Webots with file {}".format(str(output_file)))
    run_webots_with_file(output_file)

    # Read statistics in response
    with open(response_file, "rt") as fp:
        response = "".join(list(fp))
    response_dict = json.loads(response)
    myprint("Response: {}".format(str(response_dict)))

    # Store robot structure for later analysis
    robot_file = robot_file_template.format(str(i))
    with open(robot_file, 'wb') as fpb:
        pickle.dump(robot, fpb)
    myprint("Wrote {}".format(robot_file))

    del robot

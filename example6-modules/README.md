# Modules

Here we introduce modules (i.e. sub-assemblies) and a module controller list.
Module controllers are parametrized by neural networks (which are not trained
in this example), and the module controller list is a mapping between
modules to controllers.

That is, during simulation, every module of the same type uses the same neural
network for actuator control; the neural networks are passed via
message-passing from the main process to the simulation process.

The main process tells the simulation which neural network to use for which
motors, thus the simulation has no need of an understanding of modules.

Important considerations:

- No learning actually takes place.

- In this example three modules are hard-coded: a torso with arms, torso
  with wheels and a torso with no limbs.

## Module representation at the source code level

Every robot part is a `TreeNode`, which have a list of `children`. Note then
that **modules are subtrees with special nodes that are "open leaves"** (i.e.
"leaves" to which other `TreeNodes` can be appended). To make a module out of
a `TreeNode` it is enough to `compress(open_leaves)` it. This will recursively
make every children point towards the `root`, stopping the traversal at the
"open leaves".

***IMPORTANT***: the above makes modules with "open leaves" with children
impossible. For example, a module with the root as an "open leaf" will
have the root as its only node.

For ease-of-use, "open leaves" do not necessarily have to be leaves (for
compressing sub-trees even if the open leaves have children).

Additionally, each module is associated with a `typeid`, so **modules of the
same "class" have the same `typeid`**.

## New sampling functionality

- A new type of sampling was implemented: module-only sampling, where only
rules which only have modules in their right-hand sides can be chosen for
robot-expansion. This is to avoid the possibility of having motors which
do not have controllers assigned.

- Grammar module expansion: one can now expand a grammar with a module, so
that whenever a right-hand side had the module root token, a new right-hand
side is created with that token replaced with the module.

## Message passing

- Motor module-based naming: each node can be given a unique name based on the
"typeid" of their subtree root.

- Motor module-based controller assignment: this new functionality allows to
compute the `(list of motors, neural network)` module-based association, so
that we can tell the simulator which motors will be controlled with which
neural network.

# TODO

- [x] Decide if "open leaves" with children is something desirable. Decided
that it was not, for the moment at least.

- [x] Module-based sampling (by adding module-only sampling).

- [x] Module-based neural network construction and communication via
message-passing between main and webots process.

## Instructions

1. Make sure you have the normal (not snap) version of Webots.

2. `poetry install`

3. `poetry shell`

4. `python main.py`

At the end the [pictures](pictures) folder will contain the images taken during
the pipeline.

See the previous examples for details.

## Examples

Some images taken during a pipeline run:

![Example 0](./pictures/picture0.jpg)

![Example 1](./pictures/picture1.jpg)

![Example 2](./pictures/picture2.jpg)

![Example 3](./pictures/picture3.jpg)

![Example 4](./pictures/picture4.jpg)

![Example 5](./pictures/picture5.jpg)

![Example 6](./pictures/picture6.jpg)

![Example 7](./pictures/picture7.jpg)

![Example 8](./pictures/picture8.jpg)

![Example 9](./pictures/picture9.jpg)

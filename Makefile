BUILD = build

ifeq (, $(shell which snap))
	$(error "No snap in $(PATH), install snap")
endif

ifeq (, $(shell which lxc))
	echo Installing LXD. This will require sudo access.
	sudo snap install lxd
	sudo groupadd -f lxd
	sudo usermod -a -G lxd $(USER)
	sudo lxd init --auto
	echo Installed LXD. Please run make again.
	exit 1
endif

LXD_IMAGESERVER = ubuntu
LXD_IMAGENAME = 20.04
LXD_INSTANCENAME = ubuntuinst

LXD_IP = $(shell $(LXC_RUN) -- ip addr show eth0 | grep "inet\b" | awk '{print $$2}' | cut -d/ -f1)

LXC_RUN = lxc exec $(LXD_INSTANCENAME)

LXD_SELF_DIR = modular-robot-synthesis

.PHONY: default clean install_lxd uninstall_lxd create_instance delete_instance

default: install_webots copy_self_to_instance

run_webots: start_instance
	@echo Running webots in $(LXD_INSTANCENAME) LXD instance.
	xhost +
	lxc exec ubuntuinst -- bash -c "export DISPLAY=:0.0"
	lxc exec ubuntuinst -- webots --no-sandbox

shell_instance: start_instance
	@echo Running shell in $(LXD_INSTANCENAME) LXD instance with X11 tunneling.
	xhost +
	lxc exec ubuntuinst -- bash -c "export DISPLAY=:0.0"
	lxc exec ubuntuinst -- bash

copy_self_to_instance: start_instance
	@echo Copying files to $(LXD_INSTANCENAME) LXD instance.
	lxc file push -r $(shell pwd) $(LXD_INSTANCENAME)/root
	-$(LXC_RUN) mv /root/$(shell basename "$(PWD)") /root/$(LXD_SELF_DIR)
	@echo Copied files to $(LXD_INSTANCENAME) LXD instance.

install_webots: start_instance
	@echo Installing Webots in $(LXD_INSTANCENAME) LXD instance.
	$(LXC_RUN) -- bash -c "wget -qO- https://cyberbotics.com/Cyberbotics.asc | sudo apt-key add -"
	$(LXC_RUN) apt-add-repository 'deb https://cyberbotics.com/debian/ binary-amd64/'
	$(LXC_RUN) apt-get update
	$(LXC_RUN) -- apt-get -y install webots

start_instance: create_instance
	-lxc start $(LXD_INSTANCENAME)

$(BUILD)/create_instance:
	@echo Creating $(LXD_INSTANCENAME) LXD instance.
	-lxc profile create x11
	cat x11.profile | lxc profile edit x11
	-lxc launch $(LXD_IMAGESERVER):$(LXD_IMAGENAME) --profile default --profile x11 $(LXD_INSTANCENAME)
	@echo Attempting to add GPU devices to $(LXD_INSTANCENAME)
	-lxc config device add $(LXD_INSTANCENAME) gpu gpu
	mkdir -p $(BUILD)
	touch $(BUILD)/create_instance
	@echo Created $(LXD_INSTANCENAME) LXD instance.

delete_instance:
	@echo Deleting $(LXD_INSTANCENAME) LXD instance.
	-lxc delete --force $(LXD_INSTANCENAME)
	-lxc profile delete x11
	@echo Deleting $(LXD_INSTANCENAME) LXD instance.

clean: delete_instance
	$(RM) -R $(BUILD)

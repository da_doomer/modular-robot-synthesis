import enum
import typing
import collections
import random


class Terminals(enum.Enum):
    # Body
    BodyLink = enum.auto()
    BodyRollJoint = enum.auto()
    BodyRigidJoint = enum.auto()
    BodyTwistJoint = enum.auto()

    # Legs
    Connector = enum.auto()
    MountLink = enum.auto()
    LimbLinkShort = enum.auto()
    LimbLinkLong = enum.auto()
    LimbRigidJoint = enum.auto()
    LimbRollJoint = enum.auto()
    LimbKneeJoint = enum.auto()
    LimbElbowJoint = enum.auto()
    WheelJoint = enum.auto()
    Wheel = enum.auto()


class TreeNode(collections.UserList):
    """Nodes have a token attribute, and data: a list of children.

    Nodes also have an ID, which may be None, or an integer.
    """
    def __init__(self,
                 token: Terminals,
                 children: typing.List = [],
                 ID: int = None,
                 ):
        super().__init__(self)
        self.token = token
        self.data.extend(children)
        self.ID = ID

    @property
    def is_leaf(self):
        return len(self.data) == 0

    @property
    def children(self):
        return self.data

    @children.setter
    def children(self, value):
        self.data = value

    def __repr__(self):
        return "{}: {}".format(repr(self.token), repr(self.data))


# Expansion rules (i.e. given this Terminal, what are their possible children)
T = Terminals
ExpansionRules: typing.Dict[Terminals, typing.List[typing.List[Terminals]]] = {
    # Body
    T.BodyLink: [
            [],
            [T.Connector],
            [T.Connector, T.BodyRollJoint],
            [T.Connector, T.BodyRigidJoint],
            [T.Connector, T.BodyTwistJoint],
            [T.BodyRollJoint],
            [T.BodyRigidJoint],
            [T.BodyTwistJoint],
        ],
    T.BodyRollJoint: [[T.BodyLink]],
    T.BodyRigidJoint: [[T.BodyLink]],
    T.BodyTwistJoint: [[T.BodyLink]],

    # Legs
    T.Connector: [[T.MountLink]],
    T.MountLink: [  # SLIGHT MODIFICATION TO ROBOGRAMMAR IN THIS RULE
            [T.LimbLinkLong],  # Instead of MountLink -> Joint,
            [T.LimbLinkShort],  # MountLink -> LimbLink
        ],
    T.LimbLinkShort: [
            [],
            [T.LimbRigidJoint],
            [T.LimbRollJoint],
            [T.LimbKneeJoint],
            [T.LimbElbowJoint],
            [T.WheelJoint],
        ],
    T.LimbLinkLong: [
            [],
            [T.LimbRigidJoint],
            [T.LimbRollJoint],
            [T.LimbKneeJoint],
            [T.LimbElbowJoint],
            [T.WheelJoint],
        ],
    T.LimbRigidJoint: [
            [T.LimbLinkLong],
            [T.LimbLinkShort],
        ],
    T.LimbRollJoint: [
            [T.LimbLinkLong],
            [T.LimbLinkShort],
        ],
    T.LimbKneeJoint: [
            [T.LimbLinkLong],
            [T.LimbLinkShort],
        ],
    T.LimbElbowJoint: [
            [T.LimbLinkLong],
            [T.LimbLinkShort],
        ],
    T.WheelJoint: [
            [T.Wheel],
        ],
    T.Wheel: [
            [],
        ],
    }


def sample_terminal_sentence(token: Terminals) -> TreeNode:
    """Receives a Terminal token and samples a (partial) sentence from
    Robogrammar which has a the token as its root. If the token is e.g.
    a BodyLink, then the returned sentence is a complete sentence"""
    # Choose an expansion rule
    chosen_rule = random.choice(ExpansionRules[token])

    # Recursively sample terminal sentences
    children = list()
    for child in chosen_rule:
        children.append(sample_terminal_sentence(child))

    return TreeNode(token, children)

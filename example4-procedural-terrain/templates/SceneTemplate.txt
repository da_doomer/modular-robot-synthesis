#VRML_SIM R2020b utf8
WorldInfo {
  coordinateSystem "NUE"
  defaultDamping Damping {
  }
}
Viewpoint {
  orientation 1 0 0 -0.5
  position 0 1 1
  follow "robot"
  followType "Tracking Shot"
}
TexturedBackground {
  texture "empty_office"
}
TexturedBackgroundLight {
  texture "empty_office"
}
{{robot}}
{{terrain}}

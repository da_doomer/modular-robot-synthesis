# Procedural terrain

Here we place robots from Robogrammar in a procedurally generated terrain.

Just `python main.py` and open the generated file in Webots.

The project includes a dummy controller that moves every
motor in a cyclic motion.

**Caveats**: As in the previous example, the grammar here is
a slight variation of Robogrammar:

1. Mount parts connect to links, instead of connecting to joints.

2. Limbs only grow side-ways (i.e. no rules 6 or 7)

## Examples

![Example 1](./pictures/generated1.png)

*Smooth-stairs terrain.*

![Example 2](./pictures/generated2.png)

*Perlin noise terrain.*

![Example 3](./pictures/generated3.png)

*Stairs terrain.*

![Example 4](./pictures/generated4.png)

*Octaved high-scale Perlin noise terrain.*

![Example 5](./pictures/generated5.png)

*Compound stairs (up and down)*

![Example 6](./pictures/generated6.png)

*Compound stairs (down and up)*

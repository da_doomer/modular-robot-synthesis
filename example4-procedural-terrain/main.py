"""This example samples from Robogrammar and places the robot in a procedurally
generated terrain.

See README.md for details.
"""
from grammar import Terminals as T
from grammar import sample_terminal_sentence
from terrain import stairs_elevation_grid
from terrain import perlin_noise_elevation_grid
from terrain import compound_stairs_elevation_grid
from webotizer import RobotWebotizer
from webotizer import ElevationGridWebotizer
from webotizer import SceneWebotizer
from pathlib import Path
import random

output_file = Path("worlds/generated.wbt")


def random_webots_terrain() -> str:
    """Example terrain configurations."""
    choice = random.choice(range(6))
    if choice == 0:
        # Very smooth stairs
        print("Terrain: smooth stairs")
        terrain = stairs_elevation_grid(
                x_len=30,
                y_len=30,
                step_height=0.5,
                step_width=5,
                start_height=-3,
                )
        webots_terrain = ElevationGridWebotizer().elevation_grid_to_webots(
                terrain,
                x_spacing=0.5,
                z_spacing=0.5,
            )
        return webots_terrain
    elif choice == 1:
        # Medium Perlin noise
        print("Terrain: medium Perlin noise")
        terrain = perlin_noise_elevation_grid(
                x_len=100,
                y_len=100,
                scale=0.8,
                )
        webots_terrain = ElevationGridWebotizer().elevation_grid_to_webots(
                terrain,
                x_spacing=0.05,
                z_spacing=0.05,
            )
        return webots_terrain
    elif choice == 2:
        # Normal stairs
        print("Terrain: normal stairs")
        terrain = stairs_elevation_grid(
                x_len=30,
                y_len=30,
                step_height=0.2,
                step_width=5,
                start_height=-3,
                )
        webots_terrain = ElevationGridWebotizer().elevation_grid_to_webots(
                terrain,
                x_spacing=0.5,
                z_spacing=0.1,
            )
        return webots_terrain
    elif choice == 3:
        # Octaved Perlin noise
        print("Terrain: octaved Perlin noise")
        terrain = perlin_noise_elevation_grid(
                x_len=100,
                y_len=100,
                scale=1.8,
                octaves=8,
                )
        webots_terrain = ElevationGridWebotizer().elevation_grid_to_webots(
                terrain,
                x_spacing=0.05,
                z_spacing=0.05,
            )
        return webots_terrain
    elif choice == 4:
        # Up and down stairs
        print("Terrain: up and down stairs")
        terrain = compound_stairs_elevation_grid(
                x_len=30,
                y_len=30,
                step_height=0.2,
                step_width=5,
                steps_before_return=20,
                )
        webots_terrain = ElevationGridWebotizer().elevation_grid_to_webots(
                terrain,
                x_spacing=0.5,
                z_spacing=0.1,
            )
        return webots_terrain
    elif choice == 5:
        # Down and up stairs
        print("Terrain: down and up stairs")
        terrain = compound_stairs_elevation_grid(
                x_len=30,
                y_len=30,
                step_height=-0.2,
                step_width=5,
                steps_before_return=20,
                )
        webots_terrain = ElevationGridWebotizer().elevation_grid_to_webots(
                terrain,
                x_spacing=0.5,
                z_spacing=0.1,
            )
        return webots_terrain
    else:
        raise Exception("Something terribly, impossibly wrong happened.")


# Sample robot
robot = sample_terminal_sentence(T.BodyLink)
print(robot)
webots_robot: str = RobotWebotizer().astnode_to_webots(robot)

# Sample terrain
webots_terrain = random_webots_terrain()

# Generate scene with robot and terrain
webots_scene = SceneWebotizer().scene_to_webots(
        webots_robot, webots_terrain
    )

with open(output_file, "wt") as fp:
    fp.write(webots_scene)

print("wrote {}".format(output_file))

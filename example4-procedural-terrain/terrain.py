import typing
from random import gauss
from itertools import product


G = typing.TypeVar('G')


def flatten(li: typing.List[typing.List[G]]) -> typing.List[G]:
    return [item for sublist in li for item in sublist]


def stairs_elevation_grid(
        x_len: int,
        y_len: int,
        step_height: float,
        step_width: int,
        start_height: float,
        ) -> typing.List[typing.List]:
    elevation_grid = list()
    current_height = start_height
    current_step_position = 0
    for xi in range(x_len):
        elevation_grid.append([current_height]*y_len)
        current_step_position += 1
        if current_step_position == step_width:
            current_height += step_height
            current_step_position = 0
    return elevation_grid


def perlin_noise_elevation_grid(
            x_len: int,
            y_len: int,
            grid_x_len: int = 10,
            grid_y_len: int = 20,
            scale: float = 1.0,
            octaves: int = 0,
        ) -> typing.List[typing.List]:
    """Based on the code at https://en.wikipedia.org/wiki/Perlin_noise"""
    gradient = [
            [[0.0, 0.0] for _ in range(grid_y_len)] for _ in range(grid_x_len)
        ]
    dims = 2
    # Fill gradient with unit vectors
    for xi, yi in product(range(grid_x_len), range(grid_y_len)):
        vec = [gauss(0, 1) for i in range(dims)]
        mag = sum(x**2 for x in vec) ** 1/2
        gradient[xi][yi] = [x/mag for x in vec]

    # Linear interpolation
    def linear_interpolate(a0: float, a1: float, w: float) -> float:
        return (1-w)*a0 + w*a1

    # Dot product of the distance and gradient vectors
    def dot_grid_gradient(ix: int, iy: int, x: float, y: float) -> float:
        # Compute the distance vector
        dx = x-ix
        dy = y-iy

        # Compute the dot-product
        return dx*gradient[ix][iy][0] + dy*gradient[ix][iy][1]

    # Compute Perlin noise at coordinates x, y
    def _perlin_noise(x: float, y: float) -> float:
        # Determine grid cell coordinates
        x0 = int(x)
        x1 = x0 + 1
        y0 = int(y)
        y1 = y0 + 1

        # Determine interpolation weights
        sx = x - x0
        sy = y - y0

        # Interpolate between grid point gradients
        n0 = dot_grid_gradient(x0, y0, x, y)
        n1 = dot_grid_gradient(x1, y0, x, y)
        ix0 = linear_interpolate(n0, n1, sx)

        n0 = dot_grid_gradient(x0, y1, x, y)
        n1 = dot_grid_gradient(x1, y1, x, y)
        ix1 = linear_interpolate(n0, n1, sx)

        value = linear_interpolate(ix0, ix1, sy)
        return value

    perlin_noise = [[0.0 for _ in range(y_len)] for _ in range(x_len)]
    for xi, yi in product(range(x_len), range(y_len)):
        # Scale points to gradient grid
        x = xi/(x_len-1) * (grid_x_len-2)
        y = yi/(y_len-1) * (grid_y_len-2)
        perlin_noise[xi][yi] = _perlin_noise(x, y)

    # Min-max scale
    minv = min(list(flatten(perlin_noise)))
    maxv = max(list(flatten(perlin_noise)))
    for xi, yi in product(range(x_len), range(y_len)):
        perlin_noise[xi][yi] = (perlin_noise[xi][yi]-minv)/(maxv-minv)
        perlin_noise[xi][yi] = (perlin_noise[xi][yi]-0.5)*scale

    # Add octaves
    if octaves > 0:
        perlin_noise_octaves = perlin_noise_elevation_grid(
                x_len=x_len,
                y_len=y_len,
                grid_x_len=grid_x_len,
                grid_y_len=grid_y_len,
                scale=scale,
                octaves=octaves-1,
            )
        for xi, yi in product(range(x_len), range(y_len)):
            perlin_noise[xi][yi] += perlin_noise_octaves[xi][yi]

    return perlin_noise


def compound_stairs_elevation_grid(
        x_len: int,
        y_len: int,
        step_height: float,
        step_width: int,
        steps_before_return: int,
        ) -> typing.List[typing.List]:
    the_going = stairs_elevation_grid(
            x_len=x_len,
            y_len=steps_before_return*step_width,
            step_height=step_height,
            step_width=step_width,
            start_height=-steps_before_return*step_height,
        )
    the_return = list(reversed(the_going))
    return the_going + the_return

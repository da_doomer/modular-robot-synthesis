"""Dummy robot controller"""
# Native imports
from pathlib import Path
import json
import time

# Webots imports
from controller import Supervisor


def myprint(message: str) -> None:
    print("MAIN: {}".format(message))


message_file = Path("../../messages/message.json")
SUCCESS_STATUS = 0

# Open message file
with open(message_file) as fp:
    message = "".join(list(fp))
message_dict = json.loads(message)
myprint("Read {}".format(str(message_dict)))

# Read message dictionary
# File to write response to main process
response_file = Path(message_dict["response_file"])
# File to write photo
photo_file = Path(message_dict["photo_file"])
# How long to simulate the robot
simulation_seconds = message_dict["simulation_seconds"]
# Simulation loop at which the photo will be taken
photo_at_loop = message_dict["photo_at_loop"]

# Initialize response dictionary
response_dict = dict()
response_dict["fitness"] = 10.0

# Execute controller
robot = Supervisor()
timestep = int(robot.getBasicTimeStep())
motors = [robot.getDeviceByIndex(i) for i in range(robot.getNumberOfDevices())]
wall_time = 0
robot.simulationSetMode(Supervisor.SIMULATION_MODE_FAST)

# Main loop:
# - perform simulation steps for `simulation_seconds` simulated seconds
picture_taken = False
running_in_fast_mode = False
loops = 0
while robot.step(timestep) != -1 and wall_time/1000 < simulation_seconds:
    wall_time += timestep
    cycle_len = 10000
    i = float(wall_time % cycle_len)
    if i > cycle_len/2:
        i = cycle_len-i
    i = i*2/cycle_len
    i = i-0.5
    i = i*2

    # Read the sensors:
    # Enter here functions to read sensor data, like:
    #  val = ds.getValue()

    # Process sensor data here.

    # Enter here functions to send actuator commands, like:
    for i_m, motor in enumerate(motors):
        dir = 1 if i_m % 2 == 1 else -1
        motor.setPosition(0.5*i*dir)

    # Hack to get the pictures to export correctly:
    # Take pictures for the 10 previous loops at which I was
    # supposed to take the photo.
    # This is so Webots can render graphics, as they were
    # disabled (simulation was running in fast mode).
    if loops == photo_at_loop-10:
        robot.simulationSetMode(Supervisor.SIMULATION_MODE_REAL_TIME)
    if loops > photo_at_loop-10 and loops < photo_at_loop:
        robot.exportImage(str(photo_file), 99)
    if loops == photo_at_loop:
        robot.simulationSetMode(Supervisor.SIMULATION_MODE_FAST)

    loops += 1

# Write response message (here using dummy statistics)
response_message = json.dumps(response_dict)
with open(response_file, "wt") as fp:
    fp.write(response_message)

# Quit simulation
robot.simulationQuit(SUCCESS_STATUS)

"""This class looks for (some) properties in PROTO files and
encapsulates them in a dict."""
import collections
from pathlib import Path


properties = ["height", "radius", "size"]


class PROTOFile(collections.UserDict):
    def __init__(self, filepath: Path):
        super().__init__(self)
        self.name = filepath.with_suffix('').name
        with open(filepath) as f:
            for line in f:
                line = line.strip()
                if len(line) == 0:
                    continue
                line_split = line.split()
                if len(line_split) == 0:
                    continue
                if line_split[0] not in properties:
                    continue
                try:
                    self[line_split[0]] = tuple(map(float, line_split[1:]))
                except Exception:
                    continue

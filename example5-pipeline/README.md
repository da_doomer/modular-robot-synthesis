# Pipeline

Here we execute an example Webots optimization pipeline using
Robogrammar robots and procedural terrains:

Loop steps:

|   | Step                                     | Process |
|---|------------------------------------------|---------|
| 1 | Sample  robot                            | main    |
| 2 | Sample terrain                           | main    |
| 3 | Place terrain and robot in webots scene  | main    |
| 4 | Write file that will be read during the simulation (e.g. weights of a neural network)        | main    |
| 5 | Launch webots process                    | main    |
| 6 | Execute simulation in fast mode          | Webots  |
| 7 | Take picture                             | Webots  |
| 8 | Write statistics (e.g. fitness)          | Webots  |
| 9 | Read statistics                          | main    |
| 10| Store robot structure for later analysis | main    |

Note that no optimization actually takes place, it is the pipeline that we show
in this example.

## Instructions

Just `python main.py` and watch the messages written to `stdout`. At the end
the [pictures](pictures) folder will contain the images taken during the
pipeline.

See the previous examples for details.

## Examples

Some images taken during a pipeline run:

![Example 0](./pictures/picture0.jpg)

![Example 1](./pictures/picture1.jpg)

![Example 2](./pictures/picture2.jpg)

![Example 3](./pictures/picture3.jpg)

![Example 4](./pictures/picture4.jpg)

![Example 5](./pictures/picture5.jpg)

![Example 6](./pictures/picture6.jpg)

![Example 7](./pictures/picture7.jpg)

![Example 8](./pictures/picture8.jpg)

![Example 9](./pictures/picture9.jpg)

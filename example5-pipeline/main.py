"""This example samples from Robogrammar and places the robot in a procedurally
generated terrain.

See README.md for details.
"""
# Native imports
import subprocess
import json
import pickle

# Local imports
from grammar import Terminals as T
from grammar import sample_terminal_sentence
from terrain import random_webots_terrain
from webotizer import RobotWebotizer
from webotizer import SceneWebotizer
from pathlib import Path

# File to write the world
output_file = Path("worlds/generated.wbt")
# File to write message to the simulator process
message_file = Path("messages/message.json")
# File to write response to the main process
response_file_template = "messages/response{}.json"
# File to write photo
photo_file_template = "pictures/picture{}.jpg"
# File to write the robot trees
robot_file_template = "messages/robot{}.pickle"
# Webots command (must be in $PATH)
webots_command = "webots"
# How long to simulate the robot
simulation_seconds = 5
# Simulation loop at which the photo will be taken
photo_at_loop = 100
# Simulations to perform
simulations = 10


def myprint(message: str) -> None:
    print("MAIN: {}".format(message))


def run_webots_with_file(filename: Path) -> None:
    """Runs webots with `filename`. Blocks until the process completes."""
    command = [webots_command, str(filename)]
    subprocess.run(command)
    return None


for i in range(simulations):
    myprint("Simulation {}/{}".format(i+1, simulations))

    # Sample robot non-trivial
    robot = sample_terminal_sentence(T.BodyLink)
    while robot.size < 5:
        robot = sample_terminal_sentence(T.BodyLink)
    myprint(str(robot))
    webots_robot: str = RobotWebotizer().astnode_to_webots(robot)

    # Sample terrain
    webots_terrain = random_webots_terrain()

    # Place terrain and robot in Webots scene
    webots_scene = SceneWebotizer().scene_to_webots(
            webots_robot, webots_terrain
        )

    with open(output_file, "wt") as fp:
        fp.write(webots_scene)
    myprint("wrote {}".format(output_file))

    # Write file that will be read during simulation
    response_file = Path(response_file_template.format(str(i)))
    photo_file = Path(photo_file_template.format(str(i)))
    message_dict = {
            "ID": 0,
            "response_file": str(response_file.resolve()),
            "photo_file": str(photo_file.resolve()),
            "simulation_seconds": simulation_seconds,
            "photo_at_loop": photo_at_loop,
        }
    message_to_robot = json.dumps(message_dict)

    with open(message_file, "wt") as fp:
        fp.write(message_to_robot)
    myprint("wrote {}".format(message_file))

    # Launch Webots process
    myprint("Running Webots with file {}".format(str(output_file)))
    run_webots_with_file(output_file)

    # Read statistics in response
    with open(response_file, "rt") as fp:
        response = "".join(list(fp))
    response_dict = json.loads(response)
    myprint("Response: {}".format(str(response_dict)))

    # Store robot structure for later analysis
    robot_file = robot_file_template.format(str(i))
    with open(robot_file, 'wb') as fpb:
        pickle.dump(robot, fpb)
    myprint("Wrote {}".format(robot_file))

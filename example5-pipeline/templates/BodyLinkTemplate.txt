Solid {
  translation {{translation}}
  rotation {{rotation}}
  children [
    BodyLinkShape {
    }
    {{children}}
  ]
  boundingObject BodyLinkShape {
  }
  physics Physics {
  }
  name "{{name}}"
}

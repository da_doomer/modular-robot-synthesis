HingeJoint {
  jointParameters HingeJointParameters {
    position 0
    axis 0 1 0
    anchor {{translation}}
  }
  device [
    RotationalMotor {
      name "{{name}}"
    }
  ]
  endPoint {{endPoint}}
}
Transform {
  rotation 0 1 0 0
  translation {{translation}}
  children [
    BodyRollJointShape {
    }
  ]
}

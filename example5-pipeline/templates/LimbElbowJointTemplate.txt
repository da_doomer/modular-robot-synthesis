HingeJoint {
  jointParameters HingeJointParameters {
    position 0
    axis 0 0 1
    anchor {{translation}}
  }
  device [
    RotationalMotor {
      name "{{name}}"
    }
  ]
  endPoint {{endPoint}}
}
Transform {
  rotation 1 0 0 1.57
  translation {{translation}}
  children [
    LimbElbowJointShape {
    }
  ]
}

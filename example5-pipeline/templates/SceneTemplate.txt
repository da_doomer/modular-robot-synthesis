#VRML_SIM R2020b utf8
WorldInfo {
  coordinateSystem "NUE"
  defaultDamping Damping {
  }
}
Viewpoint {
  fieldOfView 1.2
  orientation -0.3 -1 0 1.3
  position -2.5 1.1 0.1
  follow "robot"
  followType "Tracking Shot"
}
TexturedBackground {
  texture "empty_office"
}
TexturedBackgroundLight {
  texture "empty_office"
}
{{robot}}
{{terrain}}

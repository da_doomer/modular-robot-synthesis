"""This example shows how to convert a sentence from Robogrammar to a Webots
robot.

As this is supposed to be an example not every part has been implemented.

Place the stdout of this script to worlds/somename.wbt and open it with
Webots to see the robot.
"""
from grammar import Terminals as T
from grammar import ASTNode
from webotizer import Webotizer


# Example in Fig. 5
leg = ASTNode(T.LimbLinkShort)
leg = ASTNode(T.LimbRigidJoint, [leg])
leg = ASTNode(T.LimbLinkShort, [leg])
leg = ASTNode(T.LimbRollJoint, [leg])
leg = ASTNode(T.LimbLinkShort, [leg])
leg = ASTNode(T.LimbElbowJoint, [leg])
leg = ASTNode(T.LimbLinkShort, [leg])
leg = ASTNode(T.LimbKneeJoint, [leg])
leg = ASTNode(T.LimbLinkShort, [leg])
leg = ASTNode(T.MountLink, [leg])
leg = ASTNode(T.Connector, [leg])

legged_link = ASTNode(T.BodyLink, [leg])

link_joint = ASTNode(T.BodyRollJoint, [legged_link])
link = ASTNode(T.BodyLink, [link_joint])
link_joint = ASTNode(T.BodyRigidJoint, [link])
link = ASTNode(T.BodyLink, [link_joint])
link_joint = ASTNode(T.BodyTwistJoint, [link])

robot = ASTNode(T.BodyLink, [link_joint, leg])

webots_robot: str = Webotizer().astnode_to_webots(robot)

print(webots_robot)

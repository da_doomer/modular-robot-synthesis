"""Dummy controller"""

# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
from controller import Robot

# create the Robot instance.
robot = Robot()

# get the time step of the current world.
timestep = int(robot.getBasicTimeStep())

# You should insert a getDevice-like function in order to get the
# instance of a device of the robot. Something like:
motors = [robot.getDeviceByIndex(i) for i in range(robot.getNumberOfDevices())]

#  ds = robot.getDistanceSensor('dsname')
#  ds.enable(timestep)

wall_time = 0

# Main loop:
# - perform simulation steps until Webots is stopping the controller
while robot.step(timestep) != -1:
    wall_time += timestep
    cycle_len = 10000
    i = wall_time%cycle_len
    if i > cycle_len/2:
        i = cycle_len-i
    i = i*2/cycle_len
    i = i-0.5
    i = i*2

    # Read the sensors:
    # Enter here functions to read sensor data, like:
    #  val = ds.getValue()

    # Process sensor data here.

    # Enter here functions to send actuator commands, like:
    for i_m,motor in enumerate(motors):
        dir = 1 if i_m%2 == 1 else -1
        motor.setPosition(0.5*i*dir)

# Enter here exit cleanup code.

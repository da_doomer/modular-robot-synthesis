import enum
import typing
import collections


# We do not care about terminals in this exercise
class NonTerminals(enum.Enum):
    S = enum.auto()
    H = enum.auto()
    Y = enum.auto()
    B = enum.auto()
    T = enum.auto()
    U = enum.auto()
    E = enum.auto()
    J = enum.auto()
    L = enum.auto()


class Terminals(enum.Enum):
    # Body
    BodyLink = enum.auto()
    BodyRollJoint = enum.auto()
    BodyRigidJoint = enum.auto()
    BodyTwistJoint = enum.auto()

    # Legs
    Connector = enum.auto()
    MountLink = enum.auto()
    LimbLinkShort = enum.auto()
    LimbLinkLong = enum.auto()
    LimbRigidJoint = enum.auto()
    LimbRollJoint = enum.auto()
    LimbKneeJoint = enum.auto()
    LimbElbowJoint = enum.auto()


Token = typing.Union[NonTerminals, Terminals]


class ASTNode(collections.UserList):
    def __init__(self, token: Terminals, children: typing.List = []):
        super().__init__(self)
        self.token = token
        self.data.extend(children)

    @property
    def is_leaf(self):
        return len(self.data) == 0

    @property
    def children(self):
        return self.data

    def __repr__(self):
        return "<{}: {}".format(repr(self.token), repr(self.data))

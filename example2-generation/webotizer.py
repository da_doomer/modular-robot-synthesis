from pathlib import Path
from proto import PROTOFile
from grammar import Terminals as T
from grammar import ASTNode
import typing


protos_path = Path("protos")
templates_path = Path("templates")


def file_to_string(filepath: Path):
    with open(filepath) as f:
        s = list(f)
    return "".join(s)


protofiles: typing.Dict[T, PROTOFile] = {
    # Body
    T.BodyLink: PROTOFile(protos_path/"BodyLinkShape.proto"),
    T.BodyRollJoint: PROTOFile(protos_path/"BodyRollJointShape.proto"),
    T.BodyRigidJoint: PROTOFile(protos_path/"BodyRigidJointShape.proto"),
    T.BodyTwistJoint: PROTOFile(protos_path/"BodyTwistJointShape.proto"),

    # Legs
    T.Connector: PROTOFile(protos_path/"ConnectorShape.proto"),
    T.MountLink: PROTOFile(protos_path/"MountLinkShape.proto"),
    T.LimbLinkShort: PROTOFile(protos_path/"LimbLinkShortShape.proto"),
    T.LimbKneeJoint: PROTOFile(protos_path/"LimbKneeJointShape.proto"),
    T.LimbElbowJoint: PROTOFile(protos_path/"LimbElbowJointShape.proto"),
    T.LimbRollJoint: PROTOFile(protos_path/"LimbRollJointShape.proto"),
    T.LimbRigidJoint: PROTOFile(protos_path/"LimbRigidJointShape.proto"),
}
webots_templates: typing.Dict[T, str] = {
    # Body
    T.BodyLink: file_to_string(templates_path/"BodyLinkTemplate.txt"),
    T.BodyRollJoint: file_to_string(templates_path/"BodyRollJointTemplate.txt"),
    T.BodyRigidJoint: file_to_string(templates_path/"BodyRigidJointTemplate.txt"),
    T.BodyTwistJoint: file_to_string(templates_path/"BodyTwistJointTemplate.txt"),

    # Legs
    T.Connector: file_to_string(templates_path/"ConnectorTemplate.txt"),
    T.MountLink: file_to_string(templates_path/"MountLinkTemplate.txt"),
    T.LimbLinkShort: file_to_string(templates_path/"LimbLinkShortTemplate.txt"),
    T.LimbKneeJoint: file_to_string(templates_path/"LimbKneeJointTemplate.txt"),
    T.LimbElbowJoint: file_to_string(templates_path/"LimbElbowJointTemplate.txt"),
    T.LimbRollJoint: file_to_string(templates_path/"LimbRollJointTemplate.txt"),
    T.LimbRigidJoint: file_to_string(templates_path/"LimbRigidJointTemplate.txt"),
}
transform_template = file_to_string(templates_path/"TransformTemplate.txt")
robot_template = file_to_string(templates_path/"RobotTemplate.txt")


def format_template(template: str, **kwargs) -> str:
    # Protofiles have curly braces, which interferes with Python's str.format
    # function. We apply a simple (if inefficient) workaround.
    template = template.replace('{{', '<<<').replace('}}', '>>>')
    template = template.replace('{', '{{').replace('}', '}}')
    template = template.replace('<<<', '{').replace('>>>', '}')
    template = template.format(**kwargs)
    template = template.replace('{{', '{').replace('}}', '}')
    return template


def format_list(l: typing.List[float]) -> str:
    return " ".join(map(str,l))


def suml(a: list, b: list) -> list:
    return [ai + bi for ai, bi in zip(a,b)]


class Webotizer:
    """This class transforms ASTNodes to webots ojects keeping track of and
    naming motors so that they can be controlled."""
    def __init__(self):
        self.motors_n = 0

    def limb_joint_to_webots(
                self,
                node: ASTNode,
                translation: list = [0, 0, 0],
            ) -> str:
        # Get proto object
        proto = protofiles[node.token]
        template = webots_templates[node.token]

        # Make sure we have the correct type of child
        if len(node.children) != 1:
            raise Exception("Invalid LimbKneeJoint: did not have exactly 1 children")
        # TODO add the other type of LimbLink
        if node.children[0].token is not T.LimbLinkShort:
            raise Exception("Invalid LimbKneeJoint: did not have a LimbLink child")

        # Compute child translation
        replacements = dict()
        child_height = protofiles[node.children[0].token]['height'][0]
        if node.token is T.LimbElbowJoint:
            child_translation = suml(translation, [0, translation[0], -child_height/2])
            child_rotation = [-1, 0, 0, 1.5708]  # pi/2
        elif node.token is T.LimbRigidJoint:
            child_translation = [0, child_height/2, 0]
            child_rotation = [0, 1, 0, 0]
        else:
            child_translation = suml(translation, [0, child_height/2, 0])
            child_rotation = [0, 1, 0, 0]

        # Obtain child representation
        if node.children[0].token is T.LimbLinkShort:
            child_to_webots = self.limb_link_short_to_webots
        replacements["endPoint"] = child_to_webots(
                node.children[0],
                translation=child_translation,
                rotation=child_rotation,
            )

        # Set our translation as indicated by our parent
        replacements["translation"] = format_list(translation)

        # Increase motor count
        replacements["name"] = "asd"+ str(self.motors_n)
        self.motors_n += 1

        # Return formatted template
        template = format_template(template, **replacements)
        return template


    def body_joint_to_webots(
                self,
                node: ASTNode,
                translation: list = [0, 0, 0],
            ) -> str:
        # Get proto object
        proto = protofiles[node.token]
        template = webots_templates[node.token]

        # Make sure we have the correct type of child
        if len(node.children) != 1:
            raise Exception("Invalid BodyRollJoint: did not have exactly 1 children")
        if node.children[0].token is not T.BodyLink:
            raise Exception("Invalid BodyRollJoint: did not have a BodyLink child")

        # Compute child translation
        replacements = dict()
        child_height = protofiles[node.children[0].token]['height'][0]
        child_radius = protofiles[node.children[0].token]['radius'][0]
        child_translation = [0, child_height/2 + child_radius, 0]
        if node.token is not T.BodyRigidJoint:
            child_translation = suml(translation, child_translation)

        # Obtain child representation
        replacements["endPoint"] = self.body_link_node_to_webots(
                node.children[0],
                translation=child_translation
            )

        # Set our translation as indicated by our parent
        replacements["translation"] = format_list(translation)

        # Increase motor count
        replacements["name"] = "asd"+ str(self.motors_n)
        self.motors_n += 1

        # Return formatted template
        template = format_template(template, **replacements)
        return template


    def limb_link_short_to_webots(
                self,
                node: ASTNode,
                rotation: list = [0, 0, 1, 0],
                translation: list = [0, 0, 0],
            ) -> str:
        # Get proto object
        proto = protofiles[node.token]
        template = webots_templates[node.token]

        # If no children then replace children with empty string
        replacements = dict()
        if node.is_leaf:
            replacements["children"] = str()
        else:
            # Prepare the template children replacement list
            children = list()

            # Make sure we have at most one joint child
            knee_joint_childs = [n for n in node if n.token is T.LimbKneeJoint]
            elbow_joint_childs = [n for n in node if n.token is T.LimbElbowJoint]
            roll_joint_childs = [n for n in node if n.token is T.LimbRollJoint]
            rigid_joint_childs = [n for n in node if n.token is T.LimbRigidJoint]
            joint_childs = knee_joint_childs\
                         + elbow_joint_childs\
                         + roll_joint_childs\
                         + rigid_joint_childs
            # TODO Add the other types of joint childs
            if len(joint_childs) > 1:
                raise Exception("Invalid LimbLink, more than one Limb*Joint child.")

            # Obtain our joint child, if any, and add to children
            joint_child = (joint_childs + [None])[0]
            if joint_child is not None:
                # Translate to half our body + our radius
                child_translation = [0, proto['height'][0]/2 + proto['radius'][0], 0]
                children.append(self.limb_joint_to_webots(
                    joint_child,
                    translation=child_translation,
                ))

            # Add our children to the template replacements
            replacements["children"] = "\n".join(children)

        # Format rotation and translation as indicated by our parent
        replacements["rotation"] = format_list(rotation)
        replacements["translation"] = format_list(translation)

        # Return formatted template
        template = format_template(template, **replacements)
        return template


    def mount_link_node_to_webots(
                self,
                node: ASTNode,
            ) -> str:
        # Get proto object
        proto = protofiles[node.token]
        template = webots_templates[node.token]

        # Make sure we have the correct type of child
        # TODO add the other type of LimbLink
        if len(node.children) != 1:
            raise Exception("Invalid Connector: did not have exactly 1 children")
        if node.children[0].token is not T.LimbLinkShort:
            raise Exception("Invalid Connector: did not have a LimbLink* child")

        # Compute child translation
        replacements = dict()

        # Obtain child representation
        # TODO add the other type of LimbLink
        if node.children[0].token is T.LimbLinkShort:
            child_to_webots = self.limb_link_short_to_webots
        replacements["children"] = child_to_webots(
                node.children[0],
            )

        # Set our translation
        my_height = protofiles[node.token]['height'][0]
        translation = [0, my_height, 0]
        replacements["translation"] = format_list(translation)

        # Return formatted template
        template = format_template(template, **replacements)
        return template


    def connector_node_to_webots(
                self,
                node: ASTNode,
                rotation: list = [0, 0, 1, 0],
            ) -> str:
        # Get proto object
        proto = protofiles[node.token]
        template = webots_templates[node.token]

        # Make sure we have the correct type of child
        if len(node.children) != 1:
            raise Exception("Invalid Connector: did not have exactly 1 children")
        if node.children[0].token is not T.MountLink:
            raise Exception("Invalid Connector: did not have a MountLink child")

        # Obtain child representation
        replacements = dict()
        replacements["children"] = self.mount_link_node_to_webots(
                node.children[0]
            )

        # Set our rotation as indicated by our parent
        replacements["rotation"] = format_list(rotation)

        # Compute translation
        my_height = protofiles[node.token]['height'][0]
        translation = [0, my_height, 0]
        replacements["translation"] = format_list(translation)

        # Return formatted template
        template = format_template(template, **replacements)
        return template


    def body_link_node_to_webots(
                self,
                node: ASTNode,
                rotation: list = [0, 0, 1, 0],
                translation: list = [0, 0, 0],
            ) -> str:
        # Get proto object
        proto = protofiles[node.token]
        template = webots_templates[node.token]

        # If no children then replace children with empty string
        replacements = dict()
        if node.is_leaf:
            replacements["children"] = str()
        else:
            # Prepare the template children replacement list
            children = list()

            # Make sure we have at most one joint child
            roll_joint_childs = [n for n in node if n.token is T.BodyRollJoint]
            rigid_joint_childs = [n for n in node if n.token is T.BodyRigidJoint]
            twist_joint_childs = [n for n in node if n.token is T.BodyTwistJoint]
            joint_childs = roll_joint_childs\
                         + rigid_joint_childs\
                         + twist_joint_childs
            if len(joint_childs) > 1:
                raise Exception("Invalid LimbLink, more than one Body*Joint child.")

            # Obtain our joint child, if any, and add to children
            joint_child = (joint_childs + [None])[0]
            if joint_child is not None:
                # Translate to half our body + our radius
                child_translation = [0, proto['height'][0]/2 + proto['radius'][0], 0]
                children.append(self.body_joint_to_webots(
                    joint_child,
                    translation=child_translation,
                ))

            # Make sure we have at most one leg child
            leg_children = [l for l in node.children if l.token is T.Connector]
            if len(leg_children) > 1:
                raise Exception("Invalid LimbLink, more than one Connector child")

            # Obtain our leg child, if any, and add ("twice") to children
            leg_child = (leg_children + [None])[0]
            if leg_child is not None:
                r1 = [0, 0, 1, 1.57]  # pi/2
                r2 = [0, 0, 1, -1.57]  # -pi/2
                leg1 = self.connector_node_to_webots(
                        leg_child,
                        rotation=r1
                    )
                leg2 = self.connector_node_to_webots(
                        leg_child,
                        rotation=r2
                    )
                children.extend([leg1, leg2])

            # Add our children to the template replacements
            replacements["children"] = "\n".join(children)

        # Format rotation and translation as indicated by our parent
        replacements["rotation"] = format_list(rotation)
        replacements["translation"] = format_list(translation)

        # Return formatted template
        template = format_template(template, **replacements)
        return template


    def astnode_to_webots(self, node: ASTNode) -> str:
        template = robot_template
        robot = self.body_link_node_to_webots(node)
        template = format_template(template, **{"children":robot})
        return template

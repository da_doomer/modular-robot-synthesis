Solid {
  translation {{translation}}
  rotation {{rotation}}
  children [
    WheelShape {
    }
    {{children}}
  ]
  boundingObject WheelShape {
  }
  physics Physics {
  }
  name "{{name}}"
}

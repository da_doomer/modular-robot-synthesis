"""This example shows how to sample from Robogrammar.

The grammar here is a slight variation of Robogrammar. See README.md
"""
from grammar import Terminals as T
from grammar import TreeNode
from grammar import sample_terminal_sentence
from webotizer import Webotizer
from pathlib import Path


output_file = Path("worlds/generated.wbt")

robot = sample_terminal_sentence(T.BodyLink)
print(robot)

webots_robot: str = Webotizer().astnode_to_webots(robot)

with open(output_file, "wt") as fp:
    fp.write(webots_robot)

print("wrote {}".format(output_file))

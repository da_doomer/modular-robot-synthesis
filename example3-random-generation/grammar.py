import enum
import typing
import collections
import random


# We do not care about terminals in this exercise
class NonTerminals(enum.Enum):
    S = enum.auto()
    H = enum.auto()
    Y = enum.auto()
    B = enum.auto()
    T = enum.auto()
    U = enum.auto()
    E = enum.auto()
    J = enum.auto()
    L = enum.auto()


class Terminals(enum.Enum):
    # Body
    BodyLink = enum.auto()
    BodyRollJoint = enum.auto()
    BodyRigidJoint = enum.auto()
    BodyTwistJoint = enum.auto()

    # Legs
    Connector = enum.auto()
    MountLink = enum.auto()
    LimbLinkShort = enum.auto()
    LimbLinkLong = enum.auto()
    LimbRigidJoint = enum.auto()
    LimbRollJoint = enum.auto()
    LimbKneeJoint = enum.auto()
    LimbElbowJoint = enum.auto()
    WheelJoint = enum.auto()
    Wheel = enum.auto()


Token = typing.Union[NonTerminals, Terminals]


class TreeNode(collections.UserList):
    """Nodes have a token attribute, and data: a list of children.
    """
    def __init__(self, token: Terminals, children: typing.List = []):
        super().__init__(self)
        self.token = token
        self.data.extend(children)

    @property
    def is_leaf(self):
        return len(self.data) == 0

    @property
    def children(self):
        return self.data

    @children.setter
    def children(self, value):
        self.data = value

    def __repr__(self):
        return "{}: {}".format(repr(self.token), repr(self.data))


# Context-free Robogrammar's grammar
T = Terminals
NT = NonTerminals
Robogrammar: typing.Dict[Token, typing.List[typing.List[Token]]] = {
        # Body structure
        NT.S: [[NT.H, NT.B, NT.T]],  # r1
        NT.T: [[NT.Y, NT.B, NT.T], []],  # r2, r23
        NT.B: [[NT.U, T.Connector, T.MountLink, NT.E], [NT.U]],  # r3, r4
        NT.E: [[NT.J, NT.L, NT.E]],  # r5
        NT.U: [[T.BodyLink]],  # r8
        NT.L: [[T.LimbLinkShort]],  # r9
        # r11, r12, r13:
        NT.Y: [[T.BodyRigidJoint], [T.BodyRollJoint], [T.BodyTwistJoint]],
        # r14, r15, r16, r17
        NT.J: [[T.LimbRigidJoint], [T.LimbRollJoint], [T.LimbKneeJoint],
               [T.LimbElbowJoint]],
        # r22
        NT.H: [[]],
        # r6, r7, r20, r21
    }


# Expansion rules (i.e. given this Terminal, what are their possible children)
ExpansionRules: typing.Dict[Token, typing.List[typing.List[Token]]] = {
    # Body
    T.BodyLink: [
            [],
            [T.Connector],
            [T.Connector, T.BodyRollJoint],
            [T.Connector, T.BodyRigidJoint],
            [T.Connector, T.BodyTwistJoint],
            [T.BodyRollJoint],
            [T.BodyRigidJoint],
            [T.BodyTwistJoint],
        ],
    T.BodyRollJoint: [[T.BodyLink]],
    T.BodyRigidJoint: [[T.BodyLink]],
    T.BodyTwistJoint: [[T.BodyLink]],

    # Legs
    T.Connector: [[T.MountLink]],
    T.MountLink: [  # SLIGHT MODIFICATION TO ROBOGRAMMAR IN THIS RULE
            [T.LimbLinkLong],  # Instead of MountLink -> Joint,
            [T.LimbLinkShort],  # MountLink -> LimbLink
        ],
    T.LimbLinkShort: [
            [],
            [T.LimbRigidJoint],
            [T.LimbRollJoint],
            [T.LimbKneeJoint],
            [T.LimbElbowJoint],
            [T.WheelJoint],
        ],
    T.LimbLinkLong: [
            [],
            [T.LimbRigidJoint],
            [T.LimbRollJoint],
            [T.LimbKneeJoint],
            [T.LimbElbowJoint],
            [T.WheelJoint],
        ],
    T.LimbRigidJoint: [
            [T.LimbLinkLong],
            [T.LimbLinkShort],
        ],
    T.LimbRollJoint: [
            [T.LimbLinkLong],
            [T.LimbLinkShort],
        ],
    T.LimbKneeJoint: [
            [T.LimbLinkLong],
            [T.LimbLinkShort],
        ],
    T.LimbElbowJoint: [
            [T.LimbLinkLong],
            [T.LimbLinkShort],
        ],
    T.WheelJoint: [
            [T.Wheel],
        ],
    T.Wheel: [
            [],
        ],
    }


def sample_terminal_sentence(token: Terminals) -> TreeNode:
    """Receives a tree with possibly non terminal values. Returns a tree
    where every node is terminal"""
    # Choose an expansion rule
    chosen_rule = random.choice(ExpansionRules[token])

    # Recursively sample terminal sentences
    children = list()
    for child in chosen_rule:
        children.append(sample_terminal_sentence(child))

    return TreeNode(token, children)

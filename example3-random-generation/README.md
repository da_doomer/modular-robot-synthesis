# Sampling robots from the grammar

Here we sample from the (here equiprobable) Robogrammar grammar.

Just `python main.py` and open the generated file in Webots.

The project includes a dummy controller that moves every
motor in a cyclic motion.

**Caveats**: The grammar in this example is a slight variation of Robogrammar:

1. Mount parts connect to links, instead of connecting to joints.

2. Limbs only grow side-ways (i.e. no rules 6 or 7)

## Examples

![Example 1](./pictures/generated1.png)

![Example 2](./pictures/generated2.png)

![Example 3](./pictures/generated3.png)

![Example 4](./pictures/generated4.png)

![Example 5](./pictures/generated5.png)

![Example 6](./pictures/generated6.png)

![Example 7](./pictures/generated7.png)

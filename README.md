# modular-robot-synthesis

Learn modular-robots subassemblies to design complex robots that would otherwise be hard to design.

**This branch contains only the files needed for automatic deployment.**

## Easy installation

Pre-requisites:

 - `make`

 - [snap](https://snapcraft.io )

If you don't have LXD, we will automatically install it using snap.

Other than that this won't modify your base system. You can test it with no
worries. **Everything runs on the LXD container `ubuntuinst` so you can easily
remove it with no trace.**

Once you have snap installed in a terminal do:

```
git clone https://gitlab.com/da_doomer/modular-robot-synthesis.git
cd modular-robot-synthesis
make
```

## GPU acceleration

It is enough to have drivers installed (we use LXD's GPU passthrough).

## TODO

[ ] Figure out what the main target should do.
